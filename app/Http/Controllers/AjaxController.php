<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\User;
use DB;
use Mail;
use Auth;
class AjaxController extends Controller
{
    
    public function __construct() 
    {
    }
    public function deleteItem(Request $req)
    {
        // echo 'dfd';exit;
        $id = $req['id'];
        $model_name = '\\App\\'.$req['model'];
        $model_obj = new $model_name;
        $model_name='';
        $model_name = $model_obj->find($id);            
        $model_name->delete();
        $arr['status']='success'; 
        $arr['message']='Record deleted successfully';
        return $arr;
    }
    public function multiDeleteItem(Request $req)
    {
        $id = $req['id'];
        $req_model = $req['model'];
        $model_name = '\\App\\'.$req['model'];
        $model_obj = new $model_name;
        $id_arr = explode(",",$id);
        $datetime = \Carbon\Carbon::now();
        //-- --check property's job, if jobs are completed only thn allow to disable the property
        if($req->model=='Property')
        {
            $job = '\\App\\'.'job';
            $job_obj = new $job();
            $jobs = $job_obj::whereIn('property_id',$id_arr)->whereNull('completion_date')->count();
            if($jobs>0)
            {
                $arr['status']='error';
                $arr['message']=server_alert('complete_job_first', '', "");
                return $arr;
            }
        }
        //-- --check property's job, if jobs are completed only thn allow to disable the property
        if($req->model=='client')
        {
            $contract = '\\App\\'.'contract';
            $contract_obj = new $contract();
            $contracts = $contract_obj::whereIn('client_id',$id_arr)->whereNull('deleted_at')->count();
            if($contracts>0)
            {
                $arr['status']='error';
                $arr['message']=server_alert('delete_contract_first', '', "");
                return $arr;
            }
        }
        DB::table($req['table'])->whereIn('id', $id_arr)->update(array('deleted_at'=>$datetime,'deleted_by'=>Auth::user()->id)); 
        $table_field_log = array('client'=>'client_name',
                                'Jobletter'=>'letter_type',
                                'Property'=>'address1',
                                'Contract'=>'contract_name',
                                'Job'=>'job_number',
                                'JobLetter'=>'letter_type',
                                'JobDoc'=>'doc_name',
                                'subcontractor'=>'company_name');  
        if (array_key_exists($req['model'], $table_field_log))
        {
            $table_field_log = $table_field_log[$req['model']];
        }
        else
        {
            $table_field_log = "name";
        }        
        $model_name_log = array('JobLetter'=>'job letter',
                                'JobDoc'=>'job document',
                                'JobType'=>'job type',
                                'ApplianceType'=>'appliance type',
                                'ApplianceLocation'=>'appliance location',
                                'ApplianceMake'=>'appliance make',
                                'Job'=>'job',
                                'ApplianceModel'=>'appliance model',);  
        
        if (array_key_exists($req['model'], $model_name_log))
        {
            $model_name_log = $model_name_log[$req['model']];
        }
        else
        {
            $model_name_log = $req['model'];
        }
        $this->activity->add_activity("bulk deleted",$model_name_log,'','','','',$id);
        $arr['status']='success'; 
        $arr['message']=server_alert('delete_success', '', "");
        return $arr;
    }    
    public function changeStatus(Request $req)
    {
        $id = $req['id'];
        $status = $req['status'];
        $status_val = '';
        if($status=='A')
        {
            $status_val = 'I';
            $status_log = 'disabled';
        }
        else
        {
            $status_val = 'A';
            $status_log = 'enabled';
        }
        $model_name = '\\App\\'.$req['model'];
        $model_obj = new $model_name;
        //-- --check property's job, if jobs are completed only thn allow to disable the property
        if($req->model=='Property' && $status_val=='I')
        {
            $job = '\\App\\'.'job';
            $job_obj = new $job();
            $jobs = $job_obj::where('property_id','=',$id)->whereNull('completion_date')->count();
            if($jobs>0)
            {
                $arr['status']='error';
                $arr['message']=server_alert('complete_job_first', '', "");
                return $arr;
            }
        }
        
        $model_name = $model_obj->find($id);
        $model_name->status = $status_val;
        $model_name->save();
        $table_field_log = array('client'=>'client_name',
                                'Jobletter'=>'letter_type',
                                'finance'=>'doc_no',
                                'subcontractor'=>'company_name',
                                'Nsr'=>'sch_code',
                                'Property'=>'address1');
        if (array_key_exists($req['model'], $table_field_log))
        {
            $table_field_log = $table_field_log[$req['model']];
        }
        else
        {
            $table_field_log = "name";
        }
        
        $model_name_log = array('GroupIdentity'=> 'group identity',
                                'JobLetter'=>'job letter',
                                'Nsr'=>'nsr',
                                'JobType'=>'job type',
                                'SubJobType'=>'sub job type',
                                'ApplianceModel'=>'appliance model',
                                'ApplianceMake'=>'appliance make',
                                'ApplianceType'=>'appliance type',
                                'ApplianceLocation'=>'appliance location');  
        
        if (array_key_exists($req['model'], $model_name_log))
        {
            $model_name_log = $model_name_log[$req['model']];
        }
        else
        {
            $model_name_log = $req['model'];
        }
        $activity = new Activity();
        $activity->add_activity($status_log,$model_name_log,$id,$model_name->$table_field_log);
        $arr['status']='success';
        $arr['message']=server_alert('update_success', '', "Status");
        return $arr;
    }
    public function goToUrl(Request $req)
    {
        if($req['type']=='contract')
        {
            Session::set('client-id', $req['id']);
        }
        else if($req['type']=='purchase_order')
        {
            Session::set('PO_job_id', $req['id']);
        }
        else if($req['type']=='subcontractor_order')
        {
            Session::set('SO_job_id', $req['id']);
        }
        else if($req['type']=='finance')
        {
            Session::set('INV_job_id', $req['id']);
            Session::set('INV_type', $req['inv_type']);
        }
    }
    public function sendEmail(Request $req)
    {
            if($req->type=='PO')
            {
                $data = array();
                $user = new User(); 
                $users = $user->get_users();
                $purchase_order = new PurchaseOrder(); 
                $purchase_order = $purchase_order->getPurchaseOrder($req->id);
                $purchase_order_detail = new PurchaseOrderDetail(); 
                $purchase_order_detail = $purchase_order_detail->getPurchaseOrderDetails($purchase_order->id);
                $job = new Job(); 
                $jobs = $job->job_detail($purchase_order->job_id);
                $opeartive_id  = $purchase_order['operative_id'];
                foreach($users as $u)
                {
                    if($u['id']==$opeartive_id)
                    {
                        $purchase_order['opeartive_name'] = $u['name'];
                    }
                }
                $data['user'] = $user;
                $data['purchase_order'] = $purchase_order;
                $data['purchase_order_detail'] = $purchase_order_detail;
                $data['job'] = $jobs;
                $department = new Department();
                $departments = $department->department_detail($data['job']['department']);
                $data['department'] = $departments;
                $subcontractor = new Subcontractor();
                $suppliers = $subcontractor->getSubcontractorDetail($purchase_order->supplier);
                $data['suppliers'] = $suppliers;
                $getpdf = new GetpdfController();
                $filename = 'purchase_order_'.time();
                $pdf = $getpdf->getpdf('purchase_orders.purchase_order_pdf',$data, 'save',$filename);

            }
            else if($req->type=='SO')
            {
                $data = array();
                $user = new User(); 
                $users = $user->get_users();
                $subcontractor_order = new SubcontractorOrder(); 
                $subcontractor_order = $subcontractor_order->getSubcontractorOrder($req->id);
                $job = new Job(); 
                $jobs = $job->job_detail($subcontractor_order->job_id);
                $property = new Property(); 
                $properties = $property->property_detail($jobs->property_id);
                $property_supervisor_id  = $subcontractor_order['property_supervisor_user_id'];
                $contract_manager_id  = $subcontractor_order['contract_manager_user_id'];
                $to_id  = $subcontractor_order['to'];
                foreach($users as $u)
                {
                    if($u['id']==$property_supervisor_id)
                    {
                        $subcontractor_order['property_supervisor'] = $u['name'];
                    }
                    if($u['id']==$contract_manager_id)
                    {
                        $subcontractor_order['contract_manager'] = $u['name'];
                    }
                    if($u['id']==$to_id)
                    {
                        $subcontractor_order['to'] = $u['name'];
                    }
                }
                $data['user'] = $user;
                $data['subcontractor_order'] = $subcontractor_order;
                $data['job'] = $jobs;
                $data['property'] = $properties;
                $department = new Department();
                $departments = $department->department_detail($data['job']['department']);
                $data['department'] = $departments;
                $getpdf = new GetpdfController();
                $filename = 'subcontractor_order_'.time();
                $pdf = $getpdf->getpdf('subcontractor_orders.subcontractor_order_pdf',$data, 'save',$filename);
            }
            $data_body['subject'] = $req->subject;
            $data_body['emailBody'] = $req->emailBody;
            Mail::send('mail.purchase_or_subcontractor_order', $data_body, function($message) use ($req,$filename) {
                    $message->to([$req->get('email'), $req->get('cc')])
                            ->subject($req->subject);
                    $message->attach('pdf_temp/'.$filename.'.pdf');
                });
    }
    public function deleteImage(Request $req){
        // $art_obj = new \\App\\Art;
        $art = DB::table('arts')->select('image')->where('id',$req->id)->first();
        $tmp = explode(',',$art->image);
        unset($tmp[array_search($req->image, $tmp)]);
        $tmp = implode(',',$tmp);
        DB::table('arts')->where('id',$req->id)->update(['image'=>$tmp]);
        return response()->json(['result'=>'success']);
    }
    public function getStates(Request $req){
        $states = DB::table('states')->select('name as text', 'id as value')->where('country_id',$req->country_id)->get();
        return response()->json(['result'=>'success', 'states'=>$states]);
    }
    public function getCities(Request $req){
        $cities = DB::table('cities')->select('name as text', 'id as value')->whereIn('state_id',explode(',',$req->state_id))->get();
        return response()->json(['result'=>'success', 'cities'=>$cities]);
    }
}
