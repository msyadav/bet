<?php

namespace App\Http\Controllers;

use App\City;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use DB;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View("admin.pagination_common.index");
    }

    public function getList(Request $req)
    { 
        if(app('request')->exists('field') && Input::get('field')=='')
        {
            Session::forget('city_field');
            Session::forget('city_sort');
        }

        Session::put('city_field', Input::has('field') ? Input::get('field') : (Session::has('city_field') ? Session::get('city_field') : 'created_at'));
        Session::put('city_sort', Input::has('sort') ? Input::get('sort') : (Session::has('city_sort') ? Session::get('city_sort') : 'asc'));
        $show = Session::get('city_show');
        $search = Session::get('city_search');
        // DB::enableQueryLog(); 
        $query = City::select(['cities.*', 'states.name as state']);
        $cities = $query->leftjoin('states','cities.state_id','states.id')
                ->orderBy(Session::get('city_field'), Session::get('city_sort'))
                ->paginate(100);
        // print_r(DB::getQueryLog());exit;
        // echo '<pre>';print_r($countries->toArray());exit;
        return view('admin.city.list', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::all();
        // echo "<pre>";
        // print_r($states->toArray());
        // exit();
        return View("admin/city/add-city", ['states' => $states]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = City::create(['name'=>$request->city, 'state_id' => $request->state]);

        return redirect('admin/city')->with('success', 'City created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CIty  $cIty
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CIty  $cIty
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        $states = State::all();
        return View("admin/city/add-city", ['city' => $city, 'states' => $states]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CIty  $cIty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $city = City::updateOrCreate(
                    ['id' => $city->id],
                    ['name' => $request->city, 'state_id' => $request->state]
                );

        return redirect('admin/city')->with('success', 'City updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CIty  $cIty
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
    }
}
