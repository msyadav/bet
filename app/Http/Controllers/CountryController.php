<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use DB;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View("admin.pagination_common.index");
    }

    public function getList(Request $req)
    { 
        if(app('request')->exists('field') && Input::get('field')=='')
        {
            Session::forget('country_field');
            Session::forget('country_sort');
        }

        Session::put('country_field', Input::has('field') ? Input::get('field') : (Session::has('country_field') ? Session::get('country_field') : 'created_at'));
        Session::put('country_sort', Input::has('sort') ? Input::get('sort') : (Session::has('country_sort') ? Session::get('country_sort') : 'asc'));
        $show = Session::get('country_show');
        $search = Session::get('country_search');
        // DB::enableQueryLog(); 
        $query = Country::select();

        $countries = $query->orderBy(Session::get('country_field'), Session::get('country_sort'));

        $countries = $query->paginate(100);
        // print_r(DB::getQueryLog());exit;
        // echo '<pre>';print_r($countries->toArray());exit;
        return view('admin.country.list', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View("admin/country/add-country");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country = Country::create(['name'=>$request->country]);

        return redirect('admin/country')->with('success', 'Country created successfully'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        return View("admin/country/add-country", ['country' => $country]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        $country = Country::updateOrCreate(
                    ['id' => $country->id],
                    ['name' => $request->country]
                );

        return redirect('admin/country')->with('success', 'Country updated successfully'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //
    }
}
