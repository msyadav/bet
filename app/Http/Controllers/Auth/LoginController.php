<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use App\User;
use DB;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout','showPasswordSetForm','setPassword');
        $this->username = $this->findUsername();
    }
    public function username()
    {
        return $this->username;
    }
    public function findUsername()
    {
        $login = request()->input('username');
 
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
 
        request()->merge([$fieldType => $login]);
 
        return $fieldType;
    }
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }
    public function handleProviderCallback($service)
    {
        // $user = Socialite::driver($service)->user();
        $user = Socialite::driver ( $service )->user ();
        // echo '<pre>';print_r($user);exit;   
        $authUser = $this->findOrCreateUser($user, $service);
        if($authUser){
            Auth::login($authUser, true);

        // }else{
            // return redirect('/login/#')->with('error','User already exist with same email address : '. $user->email);
        }
        return redirect($this->redirectTo);
                // echo 'dfd';exit;
        // return view ( 'home' )->withDetails ( $user )->withService ( $service );

        // $user->token;
    }
    public function findOrCreateUser($user, $provider)
    {
        $data = $user;
        // $authUser = User::where('email', $user->email)->first();
        // $data['token'] = app(\Illuminate\Auth\Passwords\PasswordBroker::class)->createToken($authUser);
        // echo 'dfd';exit;
        $authUser = User::where('email', $user->email)->first();
        if($authUser){
            // if($authUser->provider_id == $user->id){
                return $authUser;
            // }else{
                // return false;
            // }
        }
        // $authUser = User::where('provider_id', $user->id)->first();
        // if ($authUser) {
        //     return $authUser;
        // }
        $username = str_replace(' ','-',$user->name).'-'.(DB::table('users')->max('id') + 1);
        $user = User::create([
            'username'     => $username,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id,
            'email_verified_at' => DB::raw('now()')
        ]);
        $data->username = $username;
        // echo '<pre>';print_r($user);exit;
        $this->setPasswordLink($data);

        return $user;
    }
    public function setPasswordLink($user){
        // $user = User::where ('email', $request->email)-first();
        // if ( !$user ) return redirect()->back()->withErrors(['error' => '404']);

        //create a new token to be sent to the user. 
        DB::table('password_resets')->insert([
            'email' => $user->email,
            'token' => str_random(60), //change 60 to any length you want
            'created_at' => Carbon::now()
        ]);

        $tokenData = DB::table('password_resets')->where('email', $user->email)->orderBy('created_at','desc')->first();
        // $data = (array) $user;
        $data['token'] = $tokenData->token;
        $data['email'] = $user->email;
        $data['name'] = $user->name;
        $data['username'] = $user->username;
        // echo '<pre>';print_r($data);exit;
        // $email = $user->email; // or $email = $tokenData->email;
        Mail::send('mails.set-password', $data, function($message) use($data) {
                    // $message->bcc('msyadav.88@gmail.com');
                    $message->to($data['email'], $data['name'])
                    ->subject('Your username and password info');
                    // ->setBody($body, 'text/html');
                    // $message->from('xyz@gmail.com','Virat Gandhi');
                });
        if (Mail::failures()) {
            echo 'error';
            foreach(Mail::failures() as $email_address) {
                echo " - $email_address <br />";
            }
        // return response showing failed emails
        }        
    }
    public function showPasswordSetForm($token)
    {
        $tokenData = DB::table('password_resets')->where('token', $token)->first();
        if ( !$tokenData ) return redirect()->to('home'); //redirect them anywhere you want if the token does not exist.
        return view('auth.passwords.set',compact('token'));
    }
    public function setPassword(Request $request)
    {
        $password = $request->password;
        $tokenData = DB::table('password_resets')->where('token', $request->token)->first();

        $user = User::where('email', $tokenData->email)->orderBy('created_at','desc')->first();
        // return redirect('home')->with('error','test');
        // echo '<pre>';print_r($request->toArray());exit;
        if ( !$user ){
            Session::flash('error','Token Mismatch');
            if(Auth::check()){
                return redirect('home');
            }else{
                return redirect()->to('login'); //or wherever you want
            }
        }
// echo 'dfd';exit;
        $user->password = Hash::make($password);
        $user->update(); //or $user->save();

        //do we log the user directly or let them login and try their password for the first time ? if yes 
        Auth::login($user);

        // If the user shouldn't reuse the token later, delete the token 
        DB::table('password_resets')->where('email', $user->email)->delete();
        return redirect('home')->with('success','Password updated successfully');

        // redirect where we want according to whether they are logged in or not.
    }

}
