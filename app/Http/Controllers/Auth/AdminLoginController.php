<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Auth;

class AdminLoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $redirectTo = 'admin/art';

    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('guest:admin')->except('logout');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        // echo 'dfd';exit;
        return view('auth.admin-login');
    }
    public function login(Request $request){
        // echo 'dfd';exit;
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|min:6',
            'g-recaptcha-response' => 'required|recaptcha'
        ]);
        if(Auth::guard('admin')->attempt(['username'=> $request->username, 'password' => $request->password], $request->remember)){
            return redirect()->intended('admin/art');
        }
        $errors = ['username' => trans('auth.failed')];
        return redirect()->back()->withInput($request->only('username','remember'))->withErrors($errors);
    }
    public function logout(){
        Auth::guard('admin')->logout();
        return redirect('/admin/login');
    }
}
