<?php

namespace App\Http\Controllers;
// use App\Http\Controllers\Controller; 

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('admin/arts');
        // echo 'dfd';exit;
        return view('admin.dashboard');
    }
}
