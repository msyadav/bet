<?php

namespace App\Http\Controllers;
// use App\Http\Controllers\Controller; 

use Illuminate\Http\Request;
use App\User;

class AdminDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('auth:admin',['only' => 'index','edit']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo 'dfd';exit;
        return view('admin');
    }
}
