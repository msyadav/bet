<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Art;
use App\User;
use View;
use Auth;
use Mail;
use Crypt;
use DB;
use Storage;
use Image;
class AdminArtController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {

        return View("admin.pagination_common.index");

    }

    public function art_detail(Request $req)

    {
        $art = new Art();
        $id = $req['id'];
        $art_detail = $art->art_detail($id);
        return $art_detail;
    }
    public function getList(Request $req)
    { 
        // echo 'dfd';exit;
        if(app('request')->exists('field') && Input::get('field')=='')
        {
            Session::forget('art_field');
            Session::forget('art_sort');
        }
        // Session::put('client_search', Input::has('ok') ? Input::get('search') : (Session::has('client_search') ? Session::get('client_search') : ''));
        // Session::put('client_show', Input::has('show') ? Input::get('show') : (Session::has('client_show') ? Session::get('client_show') : '20'));        
        Session::put('art_field', Input::has('field') ? Input::get('field') : (Session::has('art_field') ? Session::get('art_field') : 'created_at'));
        Session::put('art_sort', Input::has('sort') ? Input::get('sort') : (Session::has('art_sort') ? Session::get('art_sort') : 'asc'));
        $show = Session::get('art_show');
        $search = Session::get('art_search');
        DB::enableQueryLog(); 
        $query = Art::select(['arts.*',  DB::raw('group_concat(artists.name SEPARATOR ", ") as artist')]);
        // if($search != ''){   
        //     $query->where(function($query) use ($search){
        //     $query->where('arts.client_name', 'LIKE', '%'.$search.'%');
        //     $query->orwhere('arts.client_code', 'LIKE', '%'.$search.'%');
        //     $query->orwhere('arts.postcode', 'LIKE', '%'.$search.'%');
        //     $query->orwhere('arts.has_contract', 'LIKE', '%'.$search.'%');
        //     $query->orwhere('arts.name', 'LIKE', '%'.$search.'%');
        //     });  
        // }
        $arts = $query->leftjoin('art_artist','arts.id','art_artist.art_id')
                ->leftjoin('artists','art_artist.artist_id','artists.id')
                ->orderBy(Session::get('art_field'), Session::get('art_sort'))
                        //->get();echo dd(DB::getQueryLog());exit;
                ->groupBy('arts.id');
        $arts = $query->paginate(100);
        // print_r(DB::getQueryLog());exit;
        // echo '<pre>';print_r($arts->toArray());exit;
        return view('admin.arts.list', compact('arts'));
    }
    public function create()
    {
        // $names = $artists = $venues = $years = $bands = $colors = array();
        // $names1 = DB::table('names')->select(['id','name'])->get()->toArray();
        // foreach($names1 as $key => $val){
        //     $names[$val->id] = $val->name;
        // }
        // $years1 = DB::table('years')->select(['id','name'])->get()->orderBy(DB::raw('LOWER(name)'))->toArray();
        // foreach($years1 as $key => $val){
        //     $years[$val->id] = $val->name;
        // }
        $artist1 = DB::table('artists')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get()->toArray();
        foreach($artist1 as $key => $val){
            $artists[$val->id] = $val->name;
        }
        $printcategories = array('Art Print','Gig Print');
        $printtypes_gig = array('Artist Edition','Artist Proof','Original','Printer Proof','Show Edition');
        $printtypes_art = array('Artist Edition','Artist Proof','Original','Printer Proof');
        $printversions = array('Regular','Variant');
        $bands1 = DB::table('bands')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($bands1 as $key => $val){
            $bands[$val->id] = $val->name;
        }
        $venues1 = DB::table('venues')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($venues1 as $key => $val){
            $venues[$val->id] = $val->name;
        }
        $colors1 = DB::table('colors')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($colors1 as $key => $val){
            $colors[$val->id] = $val->name;
        }
        $cities1 = DB::table('cities')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($cities1 as $key => $val){
            $cities[$val->id] = $val->name;
        }
        $states1 = DB::table('states')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($states1 as $key => $val){
            $states[$val->id] = $val->name;
        }
        $status1 = DB::table('status')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($status1 as $key => $val){
            $status[$val->id] = $val->name;
        }
        $manufacturer1 = DB::table('manufacturers')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($manufacturer1 as $key => $val){
            $manufacturers[$val->id] = $val->name;
        }
        $quantities1 = DB::table('quantities')->select(['id','name'])->orderBy(name)->get();
        foreach($quantities1 as $key => $val){
            $quantities[$val->id] = $val->name;
        }
        $size_w1 = DB::table('size_w')->select(['id','name'])->orderBy(name)->get();
        foreach($size_w1 as $key => $val){
            $size_w[$val->id] = $val->name;
        }
        $size_h1 = DB::table('size_h')->select(['id','name'])->orderBy(name)->get();
        foreach($size_h1 as $key => $val){
            $size_h[$val->id] = $val->name;
        }
        $techniques1 = DB::table('techniques')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($techniques1 as $key => $val){
            $techniques[$val->id] = $val->name;
        }
        $papers1 = DB::table('papers')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($papers1 as $key => $val){
            $papers[$val->id] = $val->name;
        }
        $events1 = DB::table('events')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($events1 as $key => $val){
            $events[$val->id] = $val->name;
        }
        $keywords1 = DB::table('keywords')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($keywords1 as $key => $val){
            $keywords[$val->id] = $val->name;
        }
        $original_prices1 = DB::table('original_prices')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($original_prices1 as $key => $val){
            $original_prices[$val->id] = $val->name;
        }
        $series1 = DB::table('series')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($series1 as $key => $val){
            $series[$val->id] = $val->name;
        }
        $num_series1 = DB::table('num_series')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($num_series1 as $key => $val){
            $num_series[$val->id] = $val->name;
        }
        $printings = array(1,2,3,4,5,6,7,8,9);
        // echo '<pre>';print_r($users);exit;
        return View("admin/arts/add-art",compact('names','years','artists','printcategories','printtypes_art','printtypes_gig','printversions','bands','venues','colors','cities','status','manufacturers','quantities','size_w','size_h','techniques','papers','events','keywords','original_prices','series','num_series','printings'));
    }
    public function store(Request $req)
    {
        ini_set('memory_limit',-1);
        // echo "dfd";exit;
        $art = new art();
        $art->add($req);
        return redirect('/admin/art')->with('alert-success', "Art Inserted successfully"); 
    }
    public function edit($id)
    {
        $artists = $venues = $years = $bands = $colors = array();
        // echo 'dfd';exit;
        $art = new art();
        $detail = $art->detail($id);
        // echo '<pre>';print_r($detail);exit;
        if (is_null($detail)) 
        {
            return redirect(URL('').'/admin/art')->with('alert-danger','Data not found');
        }

        // $user = new User();
        // $names1 = DB::table('names')->select(['id','name'])->get()->toArray();
        // foreach($names1 as $key => $val){
        //     $names[$val->id] = $val->name;
        // }
        // $years1 = DB::table('years')->select(['id','name'])->get()->toArray();
        // foreach($years1 as $key => $val){
        //     $years[$val->id] = $val->name;
        // }
        $artist1 = DB::table('artists')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get()->toArray();
        foreach($artist1 as $key => $val){
            $artists[$val->id] = $val->name;
        }
        $printcategories = array('Art Poster','Concert Poster','Event Poster','Movie Poster');
        $printtypes_gig = array('Artist Edition','Artist Proof','Original','Printer Proof','Show Edition');
        $printtypes_art = array('Artist Edition','Artist Proof','Original','Printer Proof');
        $printversions = array('Regular','Variant');
        $bands1 = DB::table('bands')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($bands1 as $key => $val){
            $bands[$val->id] = $val->name;
        }
        $venues1 = DB::table('venues')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($venues1 as $key => $val){
            $venues[$val->id] = $val->name;
        }
        $colors1 = DB::table('colors')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($colors1 as $key => $val){
            $colors[$val->id] = $val->name;
        }
        $countries1 = DB::table('countries')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($countries1 as $key => $val){
            $countries[$val->id] = $val->name;
        }
        $cities1 = DB::table('cities')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($cities1 as $key => $val){
            $cities[$val->id] = $val->name;
        }
        $states1 = DB::table('states')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($states1 as $key => $val){
            $states[$val->id] = $val->name;
        }
        $movietitle1 = DB::table('movietitle')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($movietitle1 as $key => $val){
            $movietitle[$val->id] = $val->name;
        }
        $moviegenre1 = DB::table('moviegenre')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($moviegenre1 as $key => $val){
            $moviegenre[$val->id] = $val->name;
        }
        $actor1 = DB::table('actor')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($actor1 as $key => $val){
            $actor[$val->id] = $val->name;
        }
        $status1 = DB::table('status')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        // echo '<pre>';print_r($status1);exit;
        foreach($status1 as $key => $val){
            $status[$val->id] = $val->name;
        }
        $manufacturer1 = DB::table('manufacturers')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($manufacturer1 as $key => $val){
            $manufacturers[$val->id] = $val->name;
        }
        // $quantities1 = DB::table('quantities')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        // foreach($quantities1 as $key => $val){
        //     $quantities[$val->id] = $val->name;
        // }
        for($i=1; $i<=1000; $i++){
            $quantities[$i] = $i;
        }
        $size_w1 = DB::table('size_w')->select(['id','name'])->orderBy(name)->get();
        foreach($size_w1 as $key => $val){
            $size_w[$val->id] = $val->name;
        }

        $size_h1 = DB::table('size_h')->select(['id','name'])->orderBy(name)->get();
        foreach($size_h1 as $key => $val){
            $size_h[$val->id] = $val->name;
        }
        $techniques1 = DB::table('techniques')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($techniques1 as $key => $val){
            $techniques[$val->id] = $val->name;
        }
        $papers1 = DB::table('papers')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($papers1 as $key => $val){
            $papers[$val->id] = $val->name;
        }
        $events1 = DB::table('events')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($events1 as $key => $val){
            $events[$val->id] = $val->name;
        }
        $keywords1 = DB::table('keywords')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($keywords1 as $key => $val){
            $keywords[$val->id] = $val->name;
        }
        $original_prices1 = DB::table('original_prices')->select(['id','name'])->orderBy('name')->get();
        foreach($original_prices1 as $key => $val){
            $original_prices[$val->id] = $val->name;
        }
        $series1 = DB::table('series')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($series1 as $key => $val){
            $series[$val->id] = $val->name;
        }
        $num_series1 = DB::table('num_series')->select(['id','name'])->orderBy(DB::raw('LOWER(name)'))->get();
        foreach($num_series1 as $key => $val){
            $num_series[$val->id] = $val->name;
        }
        $printings = array(1,2,3,4,5,6,7,8,9);
        // echo '<pre>';print_r($cities);exit;
        return View("admin.arts.add-art",compact('id','detail','names','years','artists','printcategories','printtypes_art','printtypes_gig','printversions','bands','venues','colors','countries','cities','states','movietitle','moviegenre','actor','status','manufacturers','quantities','size_w','size_h','techniques','papers','events','keywords','original_prices','series','num_series','printings'));
    }
    public function update(Request $req, $id)
    {
        ini_set('memory_limit',-1);
        // echo 'dfd';exit;
        $art = new art();
        $art->edit($req,$id);
        // echo 'aaa';exit;
        return redirect('/admin/art')->with('alert-success', "Art updated successfully"); 
    }
    public function uploadImage(Request $req){
        // echo '<pre>';print_r($req->toArray());
        $photos = [];
        ini_set('memory_limit',-1);
        foreach ($req->file('files') as $image) {
            // print_r($image);
                $ext = $image->getClientOriginalExtension();
                $filename = pathinfo($image->getClientOriginalName(),PATHINFO_FILENAME).'_'.time().'.'.$ext;
                $destination = public_path().'/art_images/';
                $tmp['size'] = $image->getClientSize();
                $tmp['name'] = $filename;
                $tmp['url'] = url('/art_images/'.$filename);
                $tmp['thumbnailUrl'] = url('/art_images/thumbnails/'.$filename);
                $tmp['deleteUrl'] = url('/admin/art/delete_image?file='.$filename);
                $tmp['type'] = $image->getMimeType();
                $tmp['deleteType'] = 'DELETE';
                $upload = $image->move($destination,$filename);
                $img = Image::make($destination.'/'.$filename);
                $img->resize(120, 120);
                $img->save($destination.'/thumbnails/'.$filename);
                if($upload){
                    $uploaded_filenames[] = $tmp;
                }
            // $filename = $photo->storePublicly('art_images');
            // $product_photo = ProductPhoto::create([
            //     'filename' => $filename
            // ]);

            // $photo_object = new \stdClass();
            // $photo_object->name = str_replace('photos/', '',$photo->getClientOriginalName());
            // $photo_object->size = round(Storage::size($filename) / 1024, 2);
            // $photo_object->fileID = $product_photo->id;
            // $photos[] = $photo_object;
        }

        return response()->json(array('files' => $uploaded_filenames), 200);
        // echo 'dfd';exit;
    }
    public function deleteImage(Request $req){
        // echo 'dfd';print_r($_GET);exit;
        unlink(public_path('/art_images/'.$_GET['file']));
        return response()->json(['status'=>'success']);
    }
    public function show(){

    }
}
