<?php

namespace App\Http\Controllers;

use App\State;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use DB;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View("admin.pagination_common.index");
    }

    public function getList(Request $req)
    { 
        if(app('request')->exists('field') && Input::get('field')=='')
        {
            Session::forget('state_field');
            Session::forget('state_sort');
        }

        Session::put('state_field', Input::has('field') ? Input::get('field') : (Session::has('state_field') ? Session::get('state_field') : 'created_at'));
        Session::put('state_sort', Input::has('sort') ? Input::get('sort') : (Session::has('state_sort') ? Session::get('state_sort') : 'asc'));
        $show = Session::get('country_show');
        $search = Session::get('country_search');
        // DB::enableQueryLog(); 
        $query = State::select(['states.*', 'countries.name as country']);
        $arts = $query->leftjoin('countries','states.country_id','countries.id')

                ->orderBy(Session::get('state_field'), Session::get('state_sort'));

        $states = $query->paginate(100);
        // print_r(DB::getQueryLog());exit;
        // echo '<pre>';print_r($states->toArray());exit;
        return view('admin.state.list', ['states' => $states]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return View("admin/state/add-state", ['countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $state = State::create(['name'=>$request->state, 'country_id' => $request->country]);

        return redirect('admin/state')->with('success', 'Country created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
        $countries = Country::all();
        return View("admin/state/add-state", ['countries' => $countries, 'state' => $state]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state)
    {
        $state = State::updateOrCreate(
                    ['id' => $state->id],
                    ['name' => $request->state, 'country_id' => $request->country]
                );

        return redirect('admin/state')->with('success', 'Country updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        //
    }
}
