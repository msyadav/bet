<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\Updater; 
use App\User;
use DB;
class art extends Model
{
    // use Updater;

    public static function getArts()

    {    

        $query = Art::select(['arts.*']);
        $arts =$query->get(); 
        // print_r($arts);exit;
        return $arts;
    }
    public function add($req)
    {
        // echo '<pre>';print_r($req->toArray());
        // echo '</pre>';
        $art = new Art();
        $art->name = $req->name;
        $art->year = $req->year;
        if($art->save())
        {
            $id = $art->id;
        }
        if(isset($req->artist_id)){
            $artist_id_tmp = $this->createable_multiple($req, $id, 'artist_id','artists');
            // foreach($req->artist_id as $val){
            //     if(filter_var($val, FILTER_VALIDATE_INT) == false){
            //         // echo 'dfd'.$val;exit;
            //         DB::table('artists')->insert(['name'=>$val]);
            //         $artist_id = DB::getPdo()->lastInsertId();
            //     }else{
            //         $artist_id = $val;
            //     }
            //     DB::table('art_artist')->insert(['art_id'=>$id, 'artist_id'=>$artist_id]); 
            // }
        }

        $art->title = $req->title;
        $art->year = $req->year;
        $art->printcategory = $req->printcategory;
        $art->printtype = $req->printtype;
        $art->printversion = $req->printversion;
        $art->event_id = $this->createable_single($req, 'event_id', 'events');                
        if($art->printcategory == 'Art Poster'){
            $artist = DB::table('artists')->select('name')->whereIn('id',$artist_id_tmp)->get(); 
            foreach($artist as $val){
                $tmp[] = end(explode(' ',$val->name));
            }
            $art->name = $req->title.' '.substr($req->year,2,2).' '.implode(', ',$tmp);
        }elseif($art->printcategory == 'Concert Poster'){
            $band_id_tmp = $this->createable_multiple($req, $id, 'band_id','bands');
            $city_id_tmp = $this->createable_multiple($req, $id, 'city_id','cities');
            $artist = DB::table('artists')->select('name')->whereIn('id',$artist_id_tmp)->get(); 
            foreach($artist as $val){
                $tmp[] = end(explode(' ',$val->name));
            }
            if(!empty($band_id_tmp)){
                $band = DB::table('bands')->select('name')->whereIn('id',$band_id_tmp)->get(); 
                foreach($band as $val){
                    $band_tmp[] = $val->name;
                }
                $name[] = implode(', ',$band_tmp);
            }
            if(empty($band_id_tmp) || empty($city_id_tmp)){
                $event = DB::table('events')->select('name')->where('id',$art->event_id)->first(); 
                $name[] = $event->name;
            }
            if(!empty($city_id_tmp)){
                $city = DB::table('cities')->select('name')->whereIn('id',$city_id_tmp)->get(); 
                foreach($city as $val){
                    $city_tmp[] = end(explode(' ',$val->name));
                }
                $name[] = implode(', ',$city_tmp);
            }
            $name[] = substr($req->year,2,2);
            $name[] = implode(', ',$tmp);
            $art->name = implode(' ',$name);
            // $art->name = implode(', ',$band_tmp).' - '.implode(', ',$city_tmp).' - '.substr($req->year,2,2).' - '.implode(', ',$tmp);
            $this->createable_multiple($req, $id, 'venue_id','venues');
        }
        if($art->printversion == 'Variant'){
            $this->createable_multiple($req, $id, 'color_id','colors');
        }
        $this->createable_multiple($req, $id, 'city_id','cities');
        // print_r($art->toArray());exit;
        $req->date != '' ? ($art->date = date('Y-m-d',strtotime($req->date))) : '';
        $art->image = $req->image;
        // if($req->hasFile('image')){
        //     foreach ($req->file('image') as $key => $value) {
        //         $image = $value;
        //         $ext = $image->getClientOriginalExtension();
        //         $filename = $image->getClientOriginalName().'_'.time().'.'.$ext;
        //         $destination = public_path().'/art_images/';
        //         $upload = $image->move($destination,$filename);
        //         if($upload){
        //             $uploaded_filenames[] = $filename;
        //         }
        //     }
        //     $art->image = implode(',',$uploaded_filenames);
        // }
        $art->moviegenre_id = $req->moviegenre_id;
        $art->status_id = $req->status_id;
        $this->createable_multiple($req, $id, 'manufacturer_id','manufacturers');
        $createable_single = array('movietitle_id'=>'movietitle','actor_id'=>'actor','quantity_id'=>'quantities','size_w_id'=>'size_w','size_h_id'=>'size_h', 'technique_id'=>'techniques', 'paper_id'=>'papers','original_price_id'=>'original_prices', 'series_id'=>'series');
        foreach ($createable_single as $key => $value) {
            $art->$key = $this->createable_single($req, $key, $value);
        }
        $this->createable_multiple($req, $id, 'keyword_id','keywords');
        $art->num_series_id = $req->num_series_id;
        $art->signed = $req->signed;
        $art->numbered = $req->numbered;
        $art->printings = $req->printings;
        $art->note = $req->note;
        if($art->save())
        {
            return ['id' => $art->id];
        }
    }
    public function edit($req,$id)
    {
        // echo '<pre>';print_r($req->toArray());exit;
        $art = $this->find($id);
        $image_old = $art->image;
        // DB::table('art_artist')->where('art_id',$id)->delete();
        $artist_id_tmp = $this->createable_multiple($req, $id, 'artist_id','artists');
        // if($req->name!=''){
        //     $art->name = $req->name;
        // }else{
        //     $artist = DB::table('artists')->select('name')->whereIn('id',$artist_id_tmp)->get(); 
        //     foreach($artist as $val){
        //         $tmp[] = end(explode(' ',$val->name));
        //     }
        //     // print_r($tmp);exit;
        //     $art->name = ($req->name!='' ? $req->name : implode(', ',$tmp)) . ($req->year!='' ? ' - '.substr($req->year,2,2) : '');
        // }
        // echo $art->name;exit;
        // print_r($artist);exit;
        // exit;
        $art->title = $req->title;
        $art->year = $req->year;
        $art->printcategory = $req->printcategory;
        $art->printtype = $req->printtype;
        $art->printversion = $req->printversion;
        $art->event_id = $this->createable_single($req, 'event_id', 'events');                
        if($art->printcategory == 'Art Poster'){
            if(!empty($artist_id_tmp)){
                $artist = DB::table('artists')->select('name')->whereIn('id',$artist_id_tmp)->get(); 
                foreach($artist as $val){
                    $tmp[] = end(explode(' ',$val->name));
                }
            }
            $art->name = $req->title.' '.substr($req->year,2,2).' '.implode(', ',$tmp);
        }elseif($art->printcategory == 'Concert Poster'){
            $band_id_tmp = $this->createable_multiple($req, $id, 'band_id','bands');
            $city_id_tmp = $this->createable_multiple($req, $id, 'city_id','cities');
            $artist = DB::table('artists')->select('name')->whereIn('id',$artist_id_tmp)->get(); 
            foreach($artist as $val){
                $tmp[] = end(explode(' ',$val->name));
            }
            if(!empty($band_id_tmp)){
                $band = DB::table('bands')->select('name')->whereIn('id',$band_id_tmp)->get(); 
                foreach($band as $val){
                    $band_tmp[] = $val->name;
                }
                $name[] = implode(', ',$band_tmp);
            }
            if(empty($band_id_tmp) || empty($city_id_tmp)){
                $event = DB::table('events')->select('name')->where('id',$art->event_id)->first(); 
                $name[] = $event->name;
            }
            if(!empty($city_id_tmp)){
                $city = DB::table('cities')->select('name')->whereIn('id',$city_id_tmp)->get(); 
                foreach($city as $val){
                    $city_tmp[] = end(explode(' ',$val->name));
                }
                $name[] = implode(', ',$city_tmp);
            }
            $name[] = substr($req->year,2,2);
            $name[] = implode(', ',$tmp);
            $art->name = implode(' ',$name);
            // $art->name = implode(', ',$band_tmp).' - '.implode(', ',$city_tmp).' - '.substr($req->year,2,2).' - '.implode(', ',$tmp);
            $this->createable_multiple($req, $id, 'venue_id','venues');
        }
        if($art->printversion == 'Variant'){
            $this->createable_multiple($req, $id, 'color_id','colors');
        }
        $art->country_id = $req->country_id;
        $this->createable_multiple($req, $id, 'state_id','states');
        $this->createable_multiple($req, $id, 'city_id','cities');
        // print_r($art->toArray());exit;
        $req->date != '' ? ($art->date = date('Y-m-d',strtotime($req->date))) : '';
        if($req->image != ''){
            $art->image = $image_old.','.$req->image;
        }else{
            $art->image = $req->imageinorder;
        }
        // if($req->hasFile('image')){
        //     foreach ($req->file('image') as $key => $value) {
        //         $image = $value;
        //         $ext = $image->getClientOriginalExtension();
        //         $filename = $image->getClientOriginalName().'_'.time().'.'.$ext;
        //         $destination = public_path().'/art_images/';
        //         $upload = $image->move($destination,$filename);
        //         if($upload){
        //             $uploaded_filenames[] = $filename;
        //         }
        //     }
        //     $art->image = $image_old.','.implode(',',$uploaded_filenames);
        // }else{
        //     $art->image = $req->imageinorder;
        // }
        $art->moviegenre_id = $req->moviegenre_id;
        $art->status_id = $req->status_id;
        $this->createable_multiple($req, $id, 'manufacturer_id','manufacturers');
        $createable_single = array('movietitle_id'=>'movietitle','actor_id'=>'actor','quantity_id'=>'quantities','size_w_id'=>'size_w','size_h_id'=>'size_h', 'technique_id'=>'techniques', 'paper_id'=>'papers','original_price_id'=>'original_prices', 'series_id'=>'series');
        foreach ($createable_single as $key => $value) {
            $art->$key = $this->createable_single($req, $key, $value);
            // if(strpos($req->{$key}, '#new#') !== false){
            //     DB::table($value)->insert(['name'=>str_replace('#new#', '', $req->{$key})]);
            //     $art->$key = DB::getPdo()->lastInsertId();
            // }else{
            //     $art->$key = $req->{$key};
            // }
        }
        $this->createable_multiple($req, $id, 'keyword_id','keywords');
        $art->num_series_id = $req->num_series_id;
        $art->signed = $req->signed;
        $art->numbered = $req->numbered;
        $art->printings = $req->printings;
        $art->note = $req->note;
        $art->save();
        // echo 'dfd';exit;
    }

    public function detail($id)
    {
        $art = Art::leftjoin("art_artist","arts.id","art_artist.art_id")
                // ->leftjoin("years","arts.year_id","years.id")
                ->leftjoin("art_band","arts.id","art_band.art_id")
                ->leftjoin("art_venue","arts.id","art_venue.art_id")
                ->leftjoin("art_color","arts.id","art_color.art_id")
                ->leftjoin("countries","arts.country_id","countries.id")
                ->leftjoin("art_state","arts.id","art_state.art_id")
                ->leftjoin("art_city","arts.id","art_city.art_id")
                ->leftjoin("status","arts.status_id","status.id")
                ->leftjoin("art_manufacturer","arts.id","art_manufacturer.art_id")
                ->leftjoin("quantities","arts.quantity_id","quantities.id")
                ->leftjoin("size_w","arts.size_w_id","size_w.id")
                ->leftjoin("size_h","arts.size_h_id","size_h.id")
                ->leftjoin("techniques","arts.technique_id","techniques.id")
                ->leftjoin("papers","arts.paper_id","papers.id")
                ->leftjoin("events","arts.event_id","events.id")
                ->leftjoin("art_keyword","arts.id","art_keyword.art_id")
                ->leftjoin("original_prices","arts.original_price_id","original_prices.id")
                ->leftjoin("series","arts.series_id","series.id")
                ->leftjoin("num_series","arts.num_series_id","num_series.id")
                ->where('arts.id','=',$id)
                // ->select(['*', DB::raw('group_concat(art_artist.artist_id) as artist_id')])
                ->select(['arts.*',DB::raw('group_concat(art_artist.artist_id) as artist_id'), DB::raw('group_concat(art_band.band_id) as band_id'), DB::raw('group_concat(art_venue.venue_id) as venue_id'), DB::raw('group_concat(art_color.color_id) as color_id'), DB::raw('group_concat(art_state.state_id) as state_id'), DB::raw('group_concat(art_city.city_id) as city_id'),'status.name as status_name', DB::raw('group_concat(art_manufacturer.manufacturer_id) as manufacturer_id'), DB::raw('group_concat(art_keyword.keyword_id) as keyword_id'),'original_prices.name as original_price_name','series.name as series_name','num_series.name as num_series_name'])
                ->groupBy('art_artist.art_id')
                ->first();
        return $art;
    }
    public function createable_single($req, $foreignkey, $table){
        // echo '<pre>';print_r($req->toArray());exit;
        if(strpos($req->{$foreignkey}, '#new#') !== false){
            DB::table($table)->insert(['name'=>str_replace('#new#', '', $req->{$foreignkey})]);
            $returnid = DB::getPdo()->lastInsertId();
        }else{
            $returnid = $req->{$foreignkey};
        }
        return $returnid;
    }
    public function createable_multiple($req, $id, $foreignkey, $table){
        // echo '<pre>';print_r($req->toArray());exit;
        if($id != ''){
            DB::table('art_'.str_replace('_id','',$foreignkey))->where('art_id',$id)->delete();
        }
        foreach($req->{$foreignkey} as $val){
        // if($foreignkey == 'manufacturer_id'){echo 'dfd';exit;}
            // echo $val;exit;
            if(strpos($val, '#new#') !== false){
        // echo 'dfd'.$foreignkey.' * '.$table;exit;
                DB::table($table)->insert(['name'=>str_replace('#new#', '', $val)]);
                $tmp = DB::getPdo()->lastInsertId();
            }else{
                $tmp = $val;
            }
            $ids[] = $tmp;
            // echo $tmp;exit;
            DB::table('art_'.str_replace('_id','',$foreignkey))->insert(['art_id'=>$id, $foreignkey=>$tmp]);
        }
        return $ids;
    }
}
