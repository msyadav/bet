<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['verify' => true]);
Route::get ( '/login/{service}', 'Auth\LoginController@redirectToProvider' );
Route::get ( '/login/{service}/callback', 'Auth\LoginController@handleProviderCallback' );
Route::get('password/set/{token}', 'Auth\LoginController@showPasswordSetForm')->name('password.set');
Route::post('password/set', 'Auth\LoginController@setPassword');

Route::group(['prefix' => 'admin'], function(){
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm');
    Route::post('/login', 'Auth\AdminLoginController@login');
    Route::any('/logout', 'Auth\AdminLoginController@logout');
    Route::get('/password/reset', 'AuthAdmin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    // Route::post('/password/reset', 'Auth\AdminForgotPasswordController@sendResetLinkEmail');
    Route::post('password/email', 'AuthAdmin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('password/reset/{token}', 'AuthAdmin\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('password/reset', 'AuthAdmin\ResetPasswordController@reset')->name('admin.password.update');

    Route::get('/', 'AdminController@index')->name('admin.home');

    // Route::get('arts', 'AdminController@index'); 
    Route::any('art/upload_image', 'AdminArtController@uploadImage');
    Route::any('art/delete_image', 'AdminArtController@deleteImage');
    Route::resource('/art', 'AdminArtController'); 
    Route::get('/art_list', 'AdminArtController@getList');

    Route::resource('/country', 'CountryController');
    Route::get('/country_list', 'CountryController@getList');

    Route::resource('/state', 'StateController');
    Route::get('/state_list', 'StateController@getList');

    Route::resource('/city', 'CityController');
    Route::get('/city_list', 'CityController@getList'); 

    Route::post('/deleteImage', 'AjaxController@deleteImage');
    Route::post('/delete_item', 'AjaxController@deleteItem');

    Route::post('get_states', 'AjaxController@getStates');
    Route::post('get_cities', 'AjaxController@getCities');
});

Route::group(['middleware'=>'verified'], function(){
	Route::get('/home', 'HomeController@index')->name('home');

});


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('config:cache');
    return "cache cleared";
});
Route::get('/test', 'TestController@index');
