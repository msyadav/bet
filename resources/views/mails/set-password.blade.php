<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <style type="text/css">
        .poster-btn span{background: #6a00ff; background: -moz-linear-gradient(left, #6a00ff 0%, #8900ff 100%); background: -webkit-linear-gradient(left, #6a00ff 0%,#8900ff 100%);background: linear-gradient(to right, #6a00ff 0%,#8900ff 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6a00ff', endColorstr='#8900ff',GradientType=1 ); display:inline-block; padding:8px 15px; color:#fff;
text-transform:uppercase; font-size:16px; letter-spacing:1px; font-weight:600;}
.poster-btn span font{color: #c68bff;}
    </style>
</head>
<body>
<div class="poster-btn text-center">
         <span class="b-r-6" style="width: 100%">Poster<font>drops</font></span>
      </div>
<div>
    <h3>Dear: {{ $name }}</h3>
    Thank you for choosing our platform to showcase your work.
    <br/>
    We can't wait to see what you share.
    <br/><br/><strong>Your account details:</strong>
    <br/>
    Username: {{$username}}
    <br/>
    To set your password, visit the following address: <a href="{{ url('password/set') }}/{{ $token }}">{{ url('password/set') }}/{{ $token }}</a>
    <br/><br/><strong>Regards, <br/>Posterdrops</strong>
</div>
</body>
</html>