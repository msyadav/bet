<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Posterdrops</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css_front/bootstrap.css">   
    <link rel="stylesheet" href="css_front/style.css"> 
    <link rel="stylesheet" href="css_front/login.css">  

  </head>
  <body class="login-bg">
<div class="container-fluid">
      <div class="row">
        <div class="col-12">
           <div class="login-logo pt-4"> 
              <a href="{{url('')}}"><span>POSTER</span>DROPS</a>
            </div>
        </div>
      </div>
      </div>
  <div class="login-page">
    <div class="container-fluid">
          <div class="row d-flex align-items-center login-cont">
          <div class="col-md-4 offset-md-4">
           <div class="row ">
             <div class="col-12 text-center login-logo">
               <span>POSTER</span>DROPS
             </div>
             <div class="col-12">
               <div class="login-hd text-center pb-4 pt-3">
                 <h3>Please Connect Your <span class="d-block pt-2">Posterdrops Account!</span></h3>
               </div>
             </div>
             <div class="col-12">@include('flash-message')</div>
             <div class="col-12"> 
              <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                  <input id="username" name="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" value="{{ old('username') }}" required >
                  <label class="top-t">Username</label>
                    @if ($errors->has('username'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control" id="exampleInputPassword1" required="">
                    <label class="top-t">Password</label>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
              </form>
              </div>
              <div class="col-12 pt-4 pb-4 text-center">
                  <div class="login-reg-text">
                      <a href="{{url('password/reset')}}" style="color: white" >I forgot my password.</a> <span class="d-block pt-1"><a href="{{url('register')}}" style="color: white">Register</a></span>
                  </div>
              </div>
              <div class="col-12 text-center">
                  <div class="login-social">
                      <a href="https://www.facebook.com/posterdrops/" target="_blank"><img src="images/fb-icon.svg"></a>
                      <a href="https://www.instagram.com/posterdrops/" target="_blank"><img src="images/instagram-logo.svg" width="20px"></a>
                      <a href="login/facebook" title="Login with Facebook" target="_blank"><img src="images/fb-icon.svg"></a>
                      <a href="login/google" target="_blank"><img src="images/g-icon.svg" title="Login with Google"></a>
                  </div>
              </div>
            </div>
          </div>
          </div>
    </div>    
  </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ URL::asset('js/jquery-3.3.1.slim.min.js') }}"></script>
  <!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
      
      jQuery('.login-page .form-control').bind('blur', function () {
  
        if(jQuery(this).val() == ''){
          //jQuery(this).parent().parent().removeClass('active');
          jQuery(this).closest('.login-page .form-group').removeClass('active');
        } else if(!jQuery(this).hasClass('wpcf7-select')){ 
          jQuery(this).closest('.login-page .form-group').addClass('active');
          //jQuery(this).parent().parent().addClass('active');
        }
      });

      jQuery('.login-page .form-control').bind('focus', function () {
        if(!jQuery(this).hasClass('wpcf7-select')){
          jQuery(this).closest('.login-page .form-group').addClass('active');
        }
      });

    </script>
  </body>
</html>