<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>PosterDrops - Admin Login</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{url('css_front/bootstrap.css')}}">   
    <link rel="stylesheet" href="{{url('css_front/style.css')}}"> 
    <link rel="stylesheet" href="{{url('css_front/login.css')}}">  
  <style type="text/css">
    .login-error{margin-top: 10px; background-color: white; border-left: 4px solid #dc3232; color: #444; font-size: 13px; padding: 10px; margin-left: 10px; margin-right: 10px}
    .btn-primary:disabled{background-color: #10d382}
    .btn:disabled{opacity: 0.90}
  </style>
  </head>
  <body class="login-bg">
<div class="container-fluid">
      <div class="row">
        <div class="col-12">
           <div class="login-logo pt-4"> 
              <a href="{{url('')}}"><span>POSTER</span>DROPS</a>
            </div>
        </div>
      </div>
      </div>
  <div class="login-page">
    <div class="container-fluid">
          <div class="row d-flex align-items-center login-cont">
          <div class="col-md-4 offset-md-4">
           <div class="row ">
             <div class="col-12 text-center login-logo">
               <span>POSTER</span>DROPS ADMIN LOGIN
             </div>
             <div class="col-12">&nbsp;              
             </div>
            @if (session('status'))
               <p class="alert alert-success">{{ session('status') }}</p>
            @endif
             <div class="col-12"> 
              <form method="POST" action="{{ url('/admin/login') }}">
                @csrf
                <div class="form-group">
                  <input id="username" name="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" value="{{ old('username') }}" required >
                  <label class="top-t">Username</label>
                    @if ($errors->has('username'))
                        <div class="login-error">
                            {{ $errors->first('username') }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control" id="exampleInputPassword1" required="">
                    <label class="top-t">Password</label>
                    @if ($errors->has('password'))
                        <div class="login-error">
                            {{ $errors->first('password') }}
                        </div>
                    @endif
                </div>
                <div style="margin-bottom: 1rem">
                  <div class="g-recaptcha" data-sitekey="6LdLioYUAAAAAGfXgtSRBmcaG-4wPG2fXy4fulAt" data-callback="enableSubmit"></div>
                  @if ($errors->has('g-recaptcha-response'))
                      <div class="login-error">
                          Please confirm that you are not a robot
                      </div>
                  @endif
                </div>
                <button id="submitlogin" type="submit" class="btn btn-primary btn-lg btn-block" disabled="" >Login</button>
              </form>
              </div>
              <div class="col-12 pt-4 pb-4 text-center">
                  <div class="login-reg-text">
                      <a href="{{url('admin/password/reset')}}" style="color: white" >I forgot my password.</a> <span class="d-block pt-1"></span>
                  </div>
              </div>
              <div class="col-12 text-center">
                  <div class="login-social">
                      <a href="https://www.facebook.com/posterdrops/" target="_blank"><img src="{{url('images/fb-icon.svg')}}"></a>
                      <a href="https://www.instagram.com/posterdrops/" target="_blank"><img src="{{url('images/instagram-logo.svg')}}" width="20px"></a>
                  </div>
              </div>
            </div>
          </div>
          </div>
    </div>    
  </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{url('js/jquery-3.3.1.slim.min.js')}}"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> -->
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript">
      function enableSubmit(){
        $("#submitlogin").removeAttr('disabled').removeClass('disabled');

      }
    </script>
    <script type="text/javascript">
      
      jQuery('.login-page .form-control').bind('blur', function () {
  
        if(jQuery(this).val() == ''){
          //jQuery(this).parent().parent().removeClass('active');
          jQuery(this).closest('.login-page .form-group').removeClass('active');
        } else if(!jQuery(this).hasClass('wpcf7-select')){ 
          jQuery(this).closest('.login-page .form-group').addClass('active');
          //jQuery(this).parent().parent().addClass('active');
        }
      });

      jQuery('.login-page .form-control').bind('focus', function () {
        if(!jQuery(this).hasClass('wpcf7-select')){
          jQuery(this).closest('.login-page .form-group').addClass('active');
        }
      });

    </script>
  </body>
</html>