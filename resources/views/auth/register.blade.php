<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Posterdrops</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css_front/bootstrap.css">   
    <link rel="stylesheet" href="css_front/style.css"> 
    <link rel="stylesheet" href="css_front/login.css">  

  </head>
  <body class="login-bg">
<div class="container-fluid">
      <div class="row">
        <div class="col-12">
           <div class="login-logo pt-4"> 
              <a href="{{url('')}}"><span>POSTER</span>DROPS</a>
            </div>
        </div>
      </div>
      </div>
  <div class="login-page">
    <div class="container-fluid">
          <div class="row d-flex align-items-center login-cont">
          <div class="col-md-4 offset-md-4">
           <div class="row ">
             <div class="col-12 text-center login-logo">
               <span>POSTER</span>DROPS
             </div>
             <div class="col-12">
               <div class="login-hd text-center pb-4 pt-3">
                 <h3>Please Connect Your <span class="d-block pt-2">Posterdrops Account!</span></h3>
               </div>
             </div>
             <div class="col-12"> 
              <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group">
                  <input id="username" name="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="Username" value="{{ old('username') }}" required >
                    @if ($errors->has('username'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                  <input id="email" name="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" value="{{ old('email') }}" required >
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"" placeholder="Password" required="">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation" id="password-confirm" class="form-control" placeholder="Confirm Password"  required="">
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
              </form>
              </div>
              <div class="col-12 pt-4 pb-4 text-center">
                  <div class="login-reg-text">
                      <span class="d-block pt-1"><a href="{{url('login')}}" style="color: white">Login</a></span>
                  </div>
              </div>
              <div class="col-12 text-center">
                  <div class="login-social">
                      <a href="https://www.facebook.com/posterdrops/" target="_blank"><img src="images/fb-icon.svg"></a>
                      <a href="https://www.instagram.com/posterdrops/" target="_blank"><img src="images/instagram-logo.svg" width="20px"></a>
                  </div>
              </div>
            </div>
          </div>
          </div>
    </div>    
  </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ URL::asset('js/jquery-3.3.1.slim.min.js') }}"></script>
  <!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>