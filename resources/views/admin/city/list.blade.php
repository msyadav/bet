<!--DATA TABLE HEADER-->
<style>.select2-container{width:250px !important;margin-top: 0px !important}</style>
<div class="datatable-header">
    <!-- <div id="DataTables_Table_2_filter" class="dataTables_filter">
        <label><input class="" id="search" value="{{ Session::get('user_search') }}"
                   onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('city_list')}}?ok=1&search='+this.value)"
                   placeholder="Search..."
                   type="text">
        </label>
    </div> -->
    <!-- <div id="DataTables_Table_0_filter" class="dataTables_filter">
        <button id='btn_search' type="button" class="btn  btn-sm btn-default" onclick="ajaxLoad('{{url('city_list')}}?ok=1&search='+$('#search').val()+'&user_type='+$('#user_type').val())"><i class="glyphicon glyphicon-search"></i> Go</button>
        <button type="button" id='btn_show_all' class="btn  btn-sm btn-default" onclick="ajaxLoad('{{url('city_list')}}?ok=1&search=&field=&sort=')"><i class="icon-reset"></i>  Reset</button>
    </div> -->
    <div class="dataTables_length" id="DataTables_Table_0_length">
        <label><span>Show:</span>
            <select name="show" aria-controls="DataTables_Table_2" class="show_dropdown" tabindex="-1" onchange="ajaxLoad('{{url('city_list')}}?show='+$(this).val())" >
                <option {{ (Session::get('user_show')==5)?"selected":"" }}>5</option>
                <option {{ (Session::get('user_show')==10)?"selected":"" }}>10</option>
                <option {{ (Session::get('user_show')==15)?"selected":"" }}>15</option>
                <option {{ (Session::get('user_show')==20)?"selected":"" }}>20</option>
                <option {{ (Session::get('user_show')==50)?"selected":"" }}>50</option>
                <option {{ (Session::get('user_show')==100)?"selected":"" }}>100</option>
                <option {{ (Session::get('user_show')==500)?"selected":"" }}>500</option>
            </select>
            
        </label>
    </div>
</div>
<!-- DATA TABLE CENTEr -->
    <table class="table table-togglable table-hover dataTable" id="DataTables_Table_2" role="grid" aria-describedby="DataTables_Table_2_info">
    <thead>
        <tr class="bg-blue-oakray">
            <th>#</th>
            <th rowspan="1" colspan="1" class="{{ Session::get('city_field')=='name'?(Session::get('city_sort')=='asc'?'sorting_asc':'sorting_desc'):'sorting' }}" onClick="javascript:ajaxLoad('city_list?field=name&sort={{Session::get("city_sort")=="asc"?"desc":"asc"}}')"  data-hide="x-small,small">
                    City
            </th>
            <th rowspan="1" colspan="1" class="{{ Session::get('city_field')=='name'?(Session::get('city_sort')=='asc'?'sorting_asc':'sorting_desc'):'sorting' }}" onClick="javascript:ajaxLoad('city_list?field=country_id&sort={{Session::get("city_sort")=="asc"?"desc":"asc"}}')"  data-hide="x-small,small">
                    State
            </th>
            <th rowspan="1" colspan="1" class="{{ Session::get('city_field')=='printtype'?(Session::get('city_sort')=='asc'?'sorting_asc':'sorting_desc'):'sorting' }}" onClick="javascript:ajaxLoad('city_list?field=created_at&sort={{Session::get("city_sort")=="asc"?"desc":"asc"}}')" data-toggle="true"> 
                    Created at
            </th>
            <th rowspan="1" colspan="1" class="{{ Session::get('city_field')=='printtype'?(Session::get('city_sort')=='asc'?'sorting_asc':'sorting_desc'):'sorting' }}" onClick="javascript:ajaxLoad('city_list?field=updated_at&sort={{Session::get("city_sort")=="asc"?"desc":"asc"}}')" data-toggle="true"> 
                    Updated at
            </th>
            <th rowspan="1" colspan="1" style='width:100px'>
                &nbsp;&nbsp;Action&nbsp;&nbsp;
            </th>
        </tr>
        </thead>
        <tbody>
        <?php $i = $cities->firstItem(); ?>
        @if(count($cities))    
        @foreach($cities as $key=>$city)
            <tr>
                <td>{{ $i++ }}</td>
                <td class='wrap'>{{ $city->name }}</td>
                <td class='wrap'>{{ $city->state }}</td>
                <td class='wrap'>{{ $city->created_at }}</td>
                <td class='wrap'>{{ $city->updated_at }}</td>
                <td class='text-center'>
                    <ul class="icons-list">
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li data-popup="tooltip" title="Edit"><a id='btn_edit{{ $i}}' href="{{URL('')}}/admin/city/{{ $city->id }}/edit"><i class="icon-pencil7 text-primary"></i></a></li>
                        <li data-popup="tooltip" title="Delete"><a id='btn_delete{{ $i}}' href="javascript:void(0)" onclick="delete_conf('{{ $city->id }}','City','','city');"><i class="icon-trash delete text-danger"></i></a></li> 
                    </ul>
                </td>
            </tr>
        @endforeach
        @else
            <tr>
                <td colspan='10' class='text-center'>No data found.</td>
            </tr>
        @endif
        </tbody>
    </table>        

<!--  DATA TABLE FOOTER- -->
<script>
    $('.pagination a').on('click', function (event) {
        event.preventDefault();
        ajaxLoad($(this).attr('href'));
    });    
</script>       