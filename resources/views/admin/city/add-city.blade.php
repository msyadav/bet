@extends('admin.layouts.master')  
@section('title') {{ (isset($city) ? 'Edit' : 'Add')}} City @stop                  
@if (Session::get('alert-danger') || Session::get('alert-success') )
<script>
    @if(Session::get('alert-danger'))
        show_notification("error","{{ Session::get('alert-danger') }}");
    @endif
    @if(Session::get('alert-success'))
        show_notification("success","{{ Session::get('alert-success') }}");
    @endif
</script>
@endif
@section('content')
<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-sm-10 offset-sm-1 ">
<form method="post" action='{{ (isset($city) ? url("/admin/city/".$city->id) : url("/admin/city")) }}' id="fileupload" class="form-common form-horizontal form-validate-jquery" enctype="multipart/form-data" novalidate="novalidate" accept-charset="UTF-8"> 
   @if(isset($city)) {{ method_field('PUT') }} @endif 
   @csrf
    <div class="art-form">
      <div class="art-form-box bor-top">
        <h4 class="text-center">@yield('title')</h4>
          <div class="row">
            <div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1">
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <select id="state" name='state' class="form-control" required="required" placeholder="Select state">
                        <option value="">Select State</option>
                        @foreach($states as $key => $value)
                          <option value="{{$value->id}}" <?=($value->id == $city->state_id ? 'selected' : "" ) ?>>{{$value->name}}</option>
                        @endforeach
                        
                    </select>
                  </div>
                </div>

                <div class="col-12">
                  <div class="form-group">
                    <input type="text" class="form-control" required="" name="city" id="city" value="<?=$city->name?>" placeholder="Enter city">
                  </div>
                </div>
            </div>
          </div>
      </div>
      <div class="button-submit">
        <button type="submit" class="btn btn-primary btn btn-1 mb-3"><img src="{{ URL::asset('images/ic_check.svg')}}" alt="" class="mr-2 pr-1">Submit</button><br>
        <button type="reset" class="btn btn-primary btn btn-2 float-right"><svg width="16px" height="16px" class="sv svg-reset vam mr-2 pr1" aria-hidden="true" role="img" width="18" height="18"> <use href="#sv-reset" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#sv-reset"></use> </svg> Cancel</button>
      </div>
      {{-- <div class="poster-btn text-center">
         <span class="b-r-6">Poster<font>drops</font></span>
      </div> --}}
    </div>
</form>
</div>
@stop 
@section('css')
<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.fileupload.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.fileupload-ui.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/edit-art.css') }}">
<!-- <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/fileinput.min.css') }}"> -->

@stop

