@extends('admin.layouts.master')
@section('title') {{ (isset($country) ? 'Edit' : 'Add')}} {{Request::segment(2)}} @stop                  
@section('content')
<!-- Main content -->
<style>
.show_dropdown{
    outline: 0;
    padding: 7px 12px;
    border: 1px solid #ddd;
    border-radius: 3px;
}
</style>
<div class="content-wrapper">
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>@if(Request::segment(2) != null)<ol class="breadcrumb"><li class="active">{{ucfirst(Request::segment(2))}}</li></ol> @else {!! Breadcrumbs::render() !!}@endif</h4>
            </div>
            <?php
            if(Request::segment(1)!='activity' && Request::segment(1)!='jobs' && Request::segment(1)!='reports' && Request::segment(1)!='purchase_orders' && Request::segment(1)!='subcontractor_orders' && Request::segment(1)!='finance' && Request::segment(1)!='assign_engineer' && Request::segment(1)!='reassign_engineer' && Request::segment(1)!='oconnect_forms'){?>
            <div class="heading-elements-">
                <div class="heading-btn-group">
                    <?php 
                        $add_title =  Request::segment(2);
                        $add_title = ucwords($add_title);
                    $model_name = Request::segment(1);
                    ?>
                    <a class="btn btn-primary btn-labeled btn-sm" href="{{URL('')}}/{{Request::segment(1)}}/{{Request::segment(2)}}/create" id='btn_add_{{$model_name}}'><b><i class="icon-plus3"></i></b>@yield('title')</a>

                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <!-- /page header** -->
    @if (session('status'))
        <p class="alert alert-success">{{ session('status') }}</p>
    @endif

    <!-- Content area -->
    <div class="content" id="page_message">
        <div class="panel panel-flat">
            <div id="DataTables_Table_2_wrapper" class="dataTables_wrapper no-footer">
                <div id="content"></div>
            </div>
        </div>
    </div>
    <!-- /content area -->
</div>
<!-- /main content -->
<!-- JavaScripts -->
@stop
@section('title'){{ $add_title }}@stop
@section('jsscript')
@if (Session::get('alert-danger') || Session::get('alert-success') )
<script>
    @if(Session::get('alert-danger'))
        show_notification("error","{{ Session::get('alert-danger') }}");
    @endif
    @if(Session::get('alert-success'))
        show_notification("success","{{ Session::get('alert-success') }}");
    @endif
    @if(session('status'))
        show_notification("success","{{ session('success') }}");
    @endif

</script>
@endif
<!-- <script type="text/javascript" src="{{ URL::asset('js/jquery-1.11.2.min.js') }}"></script> -->
<meta name="csrf-token" content="{{ csrf_token() }}" />
<script>
    $(document).ready(function (){
        var tmp = @if(Request::segment(3) != null) '/{{Request::segment(3)}}/{{Request::segment(4)}}' @else '' @endif;
        ajaxLoad('{{URL("")}}/admin/{{Request::segment(2)}}_list'+ tmp);
    });
</script>
@stop
