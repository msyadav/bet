<header>
      <div class="container">
        <div class="row">
          <div class="col-sm-5 col-7">
          	<? if(Request::segment(3) == 'create' || Request::segment(4) == 'edit'){ ?>
            	<a href="{{url('admin/'.Request::segment(2))}}" class="edit-art-link"><img src="{{URL::asset('images/ic_arrow_back.svg')}}" alt="" class="mr-2"> @yield('title')</a>
        	<? } ?>
          </div>

          <div class="col-sm-7 order-sm-5 col-5">
            <div class="header-right float-right">
              <div class="email-top d-inline-block"><a href="#"><img src="{{URL::asset('images/ic_stacked_email-w.svg')}}" alt=""></a> <span>2</span></div>
              <div class="profile-top d-inline-block"><a href="#" class="user-login"><img src="https://via.placeholder.com/32x32" alt="" class="user-photo"></a>
              	<div class="profile-down">
              		<div class="user-profile-sec">
              			<img src="https://via.placeholder.com/40x40" alt="">
              			<h4>{{strtoupper(Auth::user()->username)}} <span class="d-block">{{Auth::user()->email}}</span></h4>
              		</div>
              		<ul class="profile-list">
              			<li><a href="#"><img src="{{URL::asset('images/ic_mypage.svg')}}" alt=""> My page</a></li>
              			<li><a href="#"><img src="{{URL::asset('images/baseline-settings.svg')}}" alt=""> Settings</a></li>
              			<li><a href="{{url('admin/logout')}}"><img src="{{URL::asset('images/ic_signout.svg')}}" alt=""> Sign Out</a></li>
              		</ul>
              	</div>
              	</div>	
              
            </div>
          </div>
        </div>
      </div>
    </header>