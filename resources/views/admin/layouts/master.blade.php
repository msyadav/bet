<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PosterDrops - @yield('title')</title>
    {{ HTML::style('css/custom_style.css') }}
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" el="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.theme.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.structure.min.css') }}" />
    {{ HTML::style('css/bootstrap.css') }}
    <!-- {{ HTML::style('css/core.css') }} -->
    {{ HTML::style('css/components.css') }}
    {{ HTML::style('css/colors.css') }}
    <link rel="stylesheet" type="text/css" href="https://selectize.github.io/selectize.js/css/selectize.default.css">
    <link rel="stylesheet" type="text/css" href="https://selectize.github.io/selectize.js/css/selectize.bootstrap3.css">
    <link rel="stylesheet" href="{{ URL::asset('css/yearpicker.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/icons/icomoon/styles.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/icons/fontawesome/styles.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" />
<!--    OR Using HTML class   ---->
<!--        {{ HTML::style('css/icons/icomoon/styles.css') }}-->
<style type="text/css">
    .yearpicker-container{z-index: 2; font-weight: 350; font-family: Roboto}
    .yearpicker-prev, .yearpicker-next{padding: 0 20px}
    .yearpicker-items.disabled{pointer-events: none; opacity: 0.7}
    .ui-datepicker{z-index: 2 !important}
</style>
    @yield('css')

	<script type="text/javascript">
		var BASE_URL = '{{url('/admin/')}}';
	</script>
<!-- <script>
var BASE_URL = "{{ url('') }}/";
$(".modal").modal({backdrop: 'static', keyboard: false});
$( document ).ready(function() {
    validateFormByFormId("email_editor_form");
});
</script> -->
</head>
<body class="edit-art">   
    <div class="loading"></div>
    <div class="modal" data-backdrop="static" id="email_editor">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                {{ Form::open(array('class' => 'form-horizontal','id' => 'email_editor_form')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross2" style="color:white" ></i></button>
                    <h5 class="modal-title">E-Mail Editor</h5>
                </div>                
                
                <div class="modal-footer text-center">  
                    <button type="button" class="btn btn-sm bg-slate-800" id="email-editor-btn" data-resource="faq"  onclick="add_item_common('appliance_types')" ><i class="icon-mail5 position-left"></i>Send</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="icon-cross2 position-left" style="font-size:11px"></i>Cancel</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    
    <div class="modal" data-backdrop="static" id="delete_confirm">
        <div class="modal-dialog modal-xs border-slate-800">
            <div class="modal-content border-slate border-lg4">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross2" style="color:white"></i></button>
                    <h5 class="modal-title ">Delete Confirmation</h5>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this <span id="module_title"></span>?</p><br>

					 <div style="width: 100%;" class="text-center">                   
                        <button type="button" class="btn btn-xs bg-slate-800" id="delete-btn">
                        <i class="icon-bin position-left" style="font-size:11px"></i>Delete
                        </button>&nbsp;
                        <button type="button" class="btn btn-xs btn-default" data-dismiss="modal">
                            <i class="icon-cross2 position-left" style="font-size:11px"></i>Cancel
                        </button>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
    
    <div class="modal" data-backdrop="static" id="multi_delete_confirm">
        <div class="modal-dialog modal-xs border-slate-800">
            <div class="modal-content border-slate border-lg4">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross2" style="color:white"></i></button>
                    <h5 class="modal-title">Delete Confirmation</h5>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete selected items?</p> <br>
                    <input type='hidden' id='multi_del_id_arr'>
                </div>
				 <div style="width: 100%;" class="text-center">  
                    <button type="button" class="btn btn-xs bg-slate-800" id="multi_delete-btn"><i class="icon-bin position-left" style="font-size:11px"></i>Delete</button>
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><i class="icon-cross2 position-left" style="font-size:11px"></i>Cancel</button>
                    <br><br>
               </div>
            </div>
        </div>
    </div>
    
    <div class="modal" data-backdrop="static" id="delete_confirm_edit">
        <div class="modal-dialog modal-xs border-slate-800">
            <div class="modal-content border-slate border-lg4">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross2" style="color:white" ></i></button>
                    <h5 class="modal-title">Delete Confirmation</h5>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this <span id="module_title"></span>?</p>
                </div>
               <div style="width: 100%;" class="text-center">  
                    <button type="button" class="btn btn-xs bg-slate-800" id="delete-btn_edit"><i class="icon-bin position-left" style="font-size:11px"></i>Delete</button>
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><i class="icon-cross2 position-left" style="font-size:11px"></i>Close</button>
                    <br><br>
                </div>
            </div>
        </div>
    </div>   
    <div class="modal" data-backdrop="static" id="back_cancel_confirm">
        <div class="modal-dialog modal-xs border-slate-800">
            <div class="modal-content border-slate border-lg4">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross2" style="color:white" ></i></button>
                    <h5 class="modal-title"><span id='back_cancel_title'></span> Confirmation</h5>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to <span id='back_cancel_type'></span>? </p>
                </div>
                 <div style="width: 100%;" class="text-center">  
                    <button type="button" class="btn btn-xs bg-slate-800" id="confirm_action_btn">Yes</button>
                    <button type="button" class="btn btn-sm btn-default" onclick="$('#back_cancel_confirm').modal('hide')">No</button>
                    <br><br>
                </div>
            </div>
        </div>
    </div>  
    <div class="modal" data-backdrop="static" id="confirm_proceed">
        <div class="modal-dialog modal-xs border-slate-800">
            <div class="modal-content border-slate border-lg4">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross2" style="color:white" ></i></button>
                    <h6 class="modal-title">Confirmation</h6>
                </div>
                <div class="modal-body">
                    <p id="confirm_proceed_msg"></p>
                </div>
               <div style="width: 100%;" class="text-center"> 
                    <button type="button" class="btn btn-xs bg-slate-800" id="confirm_proceed_btn">Yes</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" data-backdrop="static" id="inform_popup">
        <div class="modal-dialog modal-xs border-slate-800">
            <div class="modal-content border-slate border-lg4">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-cross2" style="color:white" ></i></button>
                    <h5 class="modal-title" id='inform_popup_title'></h5>
                </div>
                <div class="modal-body">
                    <p id="inform_popup_msg"></p>
               
                <div style="width: 100%;" class="text-center">   
                    <button type="button" id="inform_popup_btn" class="btn btn-xs bg-slate-800" data-dismiss="modal">Ok</button>
                </div>
			 </div>
            </div>
        </div>
    </div>
    @include('admin.layouts.header')
    <main class="mb-3 mt-4 page-middle">
      <div class="container">
        <div class="row">
            @yield('content')
        </div>
      </div>
      
    </main>    
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script> 
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/masonry.pkgd.min.js') }}"></script>
    <!-- <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script> -->
    <!-- <script type="text/javascript" src="{{ URL::asset('js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/loaders/blockui.min.js') }}"></script> --> 
    <script type="text/javascript" src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/selectize.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/yearpicker.js') }}"></script>
	<!-- <script type="text/javascript" src="{{ URL::asset('js/plugins/forms/selects/select2.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/forms/styling/uniform.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/core/libraries/jquery_ui/core.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/selects/selectboxit.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/tags/tagsinput.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/tags/tokenfield.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/inputs/touchspin.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/inputs/maxlength.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/inputs/formatter.min.js') }}"></script> -->
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/validation/validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/plugins/forms/validation/additional_methods.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ URL::asset('js/plugins/ui/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/pickers/daterangepicker.js') }}"></script> -->
    <script type="text/javascript" src="{{ URL::asset('js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/notifications/noty.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/core/libraries/jquery_ui/full.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ URL::asset('js/plugins/forms/styling/switchery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/uploaders/fileinput.min.js') }}"></script> -->
    <script type="text/javascript" src="{{ URL::asset('js/plugins/tables/footable/footable.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ URL::asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/vendor/laravel-filemanager/js/lfm.js') }}"></script> -->
    <!-- <script type="text/javascript" src="{{ URL::asset('js/plugins/pickers/color/spectrum.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/forms/wizards/steps.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/pickers/anytime.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/pickers/pickadate/picker.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/pickers/pickadate/picker.date.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/pickers/pickadate/picker.time.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/pickers/pickadate/legacy.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mask/jquery.mask.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/ui/nicescroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/picker_date.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/components_popups.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pages/layout_fixed_custom.js') }}"></script> -->
    <script type="text/javascript" src="{{ URL::asset('js/custom_js.js') }}"></script>
<script type="text/javascript">
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(".user-login").click(function(){
        $(this).next().toggle();
        // $('.profile-down').focus();
    });
    $(document).on("click", function(event){
        // alert(event.target);
        var $trigger = $(".user-login");
        if($trigger !== event.target && !$trigger.has(event.target).length && $('.user-profile-sec') !== event.target){
            $(".profile-down").hide();
        }            
    });
    $(function(){
        $('.toggle-menu').on('click', function () {
            $('.left-menu').toggleClass('active');
            $('body').toggleClass('open-menu');
        });
        $('#container').masonry({
            itemSelector : '.item',
        });
        // $('.profile-down').on('focusout', function () {
        //     alert('dfd');
        //     $(this).hide();
        // });
    });
</script>
@yield('jsscript')
<svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
 <symbol id="sv-reset"  viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
        <path d="M13.65,2.35 C12.2,0.9 10.21,0 8,0 C3.58,0 0.01,3.58 0.01,8 C0.01,12.42 3.58,16 8,16 C11.73,16 14.84,13.45 15.73,10 L13.65,10 C12.83,12.33 10.61,14 8,14 C4.69,14 2,11.31 2,8 C2,4.69 4.69,2 8,2 C9.66,2 11.14,2.69 12.22,3.78 L9,7 L16,7 L16,0 L13.65,2.35 Z" id="path-1"></path>
    </defs>
    <g id="Pages" stroke="none" stroke-width="1" fill-rule="evenodd">
        <g id="Poster-upload-form" transform="translate(-1312.000000, -774.000000)">
            <g id="buttons" transform="translate(1268.000000, 694.000000)">
                <g id="Reset" transform="translate(24.000000, 64.000000)">
                    <g id="Text-and-Icon" transform="translate(20.000000, 15.000000)">
                        <g id="ic_refresh_24px" transform="translate(0.000000, 1.000000)">
                            <mask id="mask-2">
                                <use xlink:href="#path-1"></use>
                            </mask>
                            <use id="Path" xlink:href="#path-1"></use>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</symbol>



    </defs>
</svg>  
</body>
</html>

				