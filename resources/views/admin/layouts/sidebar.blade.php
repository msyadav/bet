<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-fixed" >
    <div class="sidebar-content">
        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                        <li {{ (Request::is('admin/arts') || Request::is('admin/arts/*') ? 'class=active' : '') }}><a class='navigation_tab' href="{{url('')}}/admin/arts"><i class="icon-users"></i> <span>Arts</span></a></li>
                    
                        <!-- <li>
                            <a href="#"><i class="icon-stack2"></i> <span>Manage Users</span></a>
                            <ul>
                                    <li {{ (Request::is('users') || Request::is('users/*')  ? 'class=active' : '') }}><a class='navigation_tab' href="{{url('')}}/users"><i class="icon-users4"></i> <span>Users</span></a></li>
                                    <li {{ (Request::is('leaves') || Request::is('leaves/*')  ? 'class=active' : '') }}><a class='navigation_tab' href="{{url('')}}/leaves"><i class="icon-stop2"></i> <span>Leave</span></a></li>
                                
                            </ul>
                        </li> --> 
                </ul>
            </div>
        </div>
    <!-- /main navigation -->
    </div>
</div>
 