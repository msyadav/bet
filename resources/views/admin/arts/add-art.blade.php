@extends('admin.layouts.master')  
@section('title') {{ (isset($detail) ? 'Edit' : 'Add')}} Art @stop                  
@if (Session::get('alert-danger') || Session::get('alert-success') )
<script>
    @if(Session::get('alert-danger'))
        show_notification("error","{{ Session::get('alert-danger') }}");
    @endif
    @if(Session::get('alert-success'))
        show_notification("success","{{ Session::get('alert-success') }}");
    @endif
</script>
@endif
@section('content')
<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-sm-10 offset-sm-1 ">
<form method="post" action='{{ (isset($detail) ? url("/admin/art/$id") : url("/admin/art")) }}' id="fileupload" class="form-common form-horizontal form-validate-jquery" enctype="multipart/form-data" novalidate="novalidate" accept-charset="UTF-8"> 
   @if(isset($detail)) {{ method_field('PUT') }} @endif 
   @csrf
     <input type="hidden" name="name" id="name" value="<?=$detail->name?>">
    <div class="art-form">
      <div class="art-form-box bor-top">
        <h4 class="text-center">Art Details</h4>
          <div class="row">
            <div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1">
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <select id="printcategory" name='printcategory' class="form-control" required="required" placeholder="Select...">
                        <option value="">Select</option>
                        <? foreach($printcategories as $val){ ?>
                            <option value="<?=$val?>" <?=($val == $detail->printcategory ? 'selected' : "" ) ?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Print Category <i class=" text-danger fa far fa-asterisk"></i></span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <select id="printtype" name='printtype' class="form-control" required="required" placeholder="Select..." >
                        <option value="">Select</option>
                         <? foreach($printtypes_gig as $val){ ?>
                            <option value="<?=$val?>" <?=($val == $detail->printtype ? 'selected' : "" ) ?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Print Type<i class=" text-danger fa far fa-asterisk" ></i></span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <select id="printversion" name='printversion' class="form-control" required="required" placeholder="Select..." >
                        <option value="">Select</option>
                         <? foreach($printversions as $val){ ?>
                            <option value="<?=$val?>" <?=($val == $detail->printversion ? 'selected' : "" ) ?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Print Version<i class=" text-danger fa far fa-asterisk" ></i></span>
                  </div>
                </div>
                <div class="col-12 variant_div">
                  <div class="form-group">
                    <select  name='color_id[]' id="color_id" class="form-control" multiple="">
                        <? foreach($colors as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->color_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Color</span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <select id="artist_id" name="artist_id[]" class="form-control" multiple="multiple" required="required">
                        <? foreach($artists as $key => $val){?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->artist_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                        <option></option>
                    </select>
                    <span class="input-b-text">Artist <i class=" text-danger fa far fa-asterisk" ></i></span>
                  </div>
                </div>
                <div class="col-12 artposter_div">
                  <div class="form-group">
                    <input type="text" name="title" id="title" class="form-control" value="<?=$detail->title?>" required >
                    <span class="input-b-text">Title <i class=" text-danger fa far fa-asterisk" ></i></span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <input type="text" name="year" id="year" class="form-control yearpicker" value="<?=$detail->year?>" required >
                    <span class="input-b-text">Year <i class=" text-danger fa far fa-asterisk" ></i></span>
                  </div>
                </div>
               <div class="col-12">
                  <div class="form-group">
                    <!-- <input name="image[]" type="file" class="form-control fileinput" placeholder="Select file..."  multiple accept='image/*'  value="<?=$detail->image?>" > -->
                    <input type="hidden" name="imageinorder" id="imageinorder" value="<?=$detail->image?>">
                    <!-- <span class="input-b-text">Image <i class=" text-danger fa far fa-asterisk" ></i></span> -->
                    <ul class="sortable image_ul" style="list-style-type: none; margin-top: -25px">
                    <? $image = explode(',',$detail->image);
                    foreach($image as $value){
                     if(is_file(public_path().'/art_images/thumbnails/'.$value)){?>
                     <li id="<?=$value?>" data-image="<?=$value?>" style="margin: 10px 5px; max-width: 115px"><img src="{{url('/art_images/thumbnails/'.$value)}}" style=""><button type="button" class="move" style=""><span class=" fa fa-arrows" style="color: white;padding: 4px"></span></button><button type="button" class="close" style="background: none"><span class=" fa fa-trash" style="color: white;padding: 4px; font-size: 18px"></span></button></li>
                    <? } 
                     } ?>
                     </ul>
                  </div>
               </div>
               <div class="col-12">
                  <div class="form-group">
                     <div class="fileupload-buttonbar">
                      <div class="">
                        <input type="hidden" name="image" id="image">
                          <!-- The fileinput-button span is used to style the file input field as button -->
                          <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Add files...</span>
                              <input type="file" name="files[]" multiple>
                          </span>
                          <button type="submit" class="btn btn-primary start">
                              <i class="glyphicon glyphicon-upload"></i>
                              <span>Start upload</span>
                          </button>
                          <button type="reset" class="btn btn-warning cancel">
                              <i class="glyphicon glyphicon-ban-circle"></i>
                              <span>Cancel upload</span>
                          </button>
                          <button type="button" class="btn btn-danger delete">
                              <i class="glyphicon glyphicon-trash"></i>
                              <span>Delete</span>
                          </button>
                          <input type="checkbox" class="toggle">
                          <!-- The global file processing state -->
                          <span class="fileupload-process"></span>
                      </div>
                      <div class=" fileupload-progress fade">
                          <!-- The global progress bar -->
                          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                              <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                          </div>
                          <!-- The extended global progress state -->
                          <div class="progress-extended">&nbsp;</div>
                      </div>
                    </div>
                    <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                  </div>
               </div>
              </div>  
            </div>
          </div>
      </div>
      <div class="art-form-box">
          <div class="row">
            <div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1">
              <div class="row">
                <div class="col-12 concertposter_div">
                  <div class="form-group">
                    <select  name='band_id[]' id="band_id" class="form-control" multiple="">
                        <? foreach($bands as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->band_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Band</span>
                  </div>
                </div>
                <div class="col-12 concertposter_div">
                  <div class="form-group">
                    <select  name='event_id' id="event_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($events as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->event_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Event</span>
                  </div>
                </div>
                <div class="col-12 concertposter_div">
                  <div class="form-group">
                    <select  name='venue_id[]' id="venue_id" class="form-control" multiple="">
                        <? foreach($venues as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->venue_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Venue</span>
                  </div>
                </div>
               <div class="col-12 eventposter_div">
                  <div class="form-group">
                     <select id="country_id" name="country_id" class="form-control">
                        <option value="">Select</option>
                        <? foreach($countries as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->country_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                     </select>
                     <span class="input-b-text">Country</span>
                  </div>
               </div>
               <div class="col-12 eventposter_div">
                  <div class="form-group">
                     <select id="state_id" name="state_id[]" class="form-control" multiple="multiple">
                     </select>
                     <span class="input-b-text">State</span>
                  </div>
               </div>
               <div class="col-12 eventposter_div">
                  <div class="form-group">
                     <select id="city_id" name="city_id[]" class="form-control" multiple="multiple">
                     </select>
                     <span class="input-b-text">City</span>
                  </div>
               </div>
                <div class="col-12 eventposter_div">
                  <div class="form-group">
                    <input type="text" name="date" id="date" class="form-control datepicker" value="<?=($detail->date!='' ? date('m/d/Y',strtotime($detail->date)) : '')?>" readonly autocomplete="off" >
                    <span class="input-b-text">Date</span>
                  </div>
                </div>
               <div class="col-12 movieposter_div">
                  <div class="form-group">
                     <select id="movietitle_id" name="movietitle_id" class="form-control">
                        <option value="">Select</option>
                        <? foreach($movietitle as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->movietitle_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                     </select>
                     <span class="input-b-text">Movie Title</span>
                  </div>
               </div>
               <div class="col-12 movieposter_div">
                  <div class="form-group">
                     <select id="moviegenre_id" name="moviegenre_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($moviegenre as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->moviegenre_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                     </select>
                     <span class="input-b-text">Movie Genre</span>
                  </div>
               </div>
               <div class="col-12 movieposter_div">
                  <div class="form-group">
                     <select id="actor_id" name="actor_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($actor as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->actor_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                     </select>
                     <span class="input-b-text">Featured Actors</span>
                  </div>
               </div>
                <div class="col-12 movieposter_div">
                  <div class="form-group">
                    <input type="text" name="release_date" id="release_date" class="form-control datepicker" value="<?=($detail->release_date!='' ? date('m/d/Y',strtotime($detail->release_date)) : '')?>" readonly autocomplete="off" >
                    <span class="input-b-text">Release Date</span>
                  </div>
                </div>
               <div class="col-12">
                  <div class="form-group">
                    <select  name='status_id' id="status_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($status as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->status_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Status</span>
                  </div>
               </div>
               <div class="col-12">
                  <div class="form-group">
                    <select id="manufacturer_id" name="manufacturer_id[]" class="form-control" multiple="multiple">
                        <? foreach($manufacturers as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->manufacturer_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Printer</span>
                  </div>
               </div>
               <div class="col-12">
                  <div class="form-group">
                    <select  name='quantity_id' id="quantity_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($quantities as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->quantity_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Edition Size</span>
                  </div>
               </div>
               <div class="col-12">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <select  name='size_w_id' id="size_w_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($size_w as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->size_w_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                        <span class="input-b-text">Width</span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <select  name='size_h_id' id="size_h_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($size_h as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->size_h_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                        <span class="input-b-text">Height</span>
                      </div>
                    </div>
                  </div>
               </div>
              </div>  
            </div>
          </div>
      </div>
      <div class="art-form-box">
          <div class="row">
            <div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1">
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <select  name='technique_id' id="technique_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($techniques as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->technique_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Technique</span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <select  name='paper_id' id="paper_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($papers as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->paper_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Paper</span>
                  </div>
                </div>
                
                <div class="col-12">
                  <div class="form-group">
                    <select  name='keyword_id[]' id="keyword_id" class="form-control" multiple="multiple" >
                        <option value="">Select</option>
                        <? foreach($keywords as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->keyword_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Keywords</span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <select  name='original_price_id' id="original_price_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($original_prices as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->original_price_id)) ? 'selected' : '')?> >$<?=$val?></option>
                        <? } ?>
                    </select>
                    <span class="input-b-text">Original Price</span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <select  name='series_id' id="series_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($series as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->series_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                        <span class="input-b-text">Series</span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <select  name='num_series_id' id="num_series_id" class="form-control" >
                        <option value="">Select</option>
                        <? foreach($num_series as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->num_series_id)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                    </select>
                        <span class="input-b-text">Number in Series</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                    <select  name='signed' id="signed" class="form-control" >
                       <option value=""> Select </option>
                       <option value="No" <?=($detail->signed == 'No' ? 'selected' : '')?> > No </option>
                       <option value="Yes"  <?=($detail->signed == 'Yes' ? 'selected' : '')?> >Yes</option>
                    </select>
                    <span class="input-b-text">Signed</span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                     <select name="numbered" id="numbered" class="form-control">
                        <option value=""> Select </option>
                        <option value="No" <?=($detail->numbered == 'No' ? 'selected' : '')?> > No </option>
                        <option value="Yes"  <?=($detail->numbered == 'Yes' ? 'selected' : '')?> >Yes</option>
                     </select>
                    <span class="input-b-text">Numbered</span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                     <select id="printings" name="printings" class="form-control">
                        <? foreach($printings as $key => $val){ ?>
                            <option value="<?=$key?>" <?=(in_array($key,explode(',', $detail->printings)) ? 'selected' : '')?> ><?=$val?></option>
                        <? } ?>
                     </select>
                    <span class="input-b-text">Printings</span>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-group">
                     <textarea name="note" id="note" class="form-control" ><?=$detail->note?></textarea>
                     <span class="input-b-text">Note</span>
                  </div>
                </div>
              </div>  
            </div>
          </div>
      </div>
      <div class="button-submit">
        <button type="submit" class="btn btn-primary btn btn-1 mb-3"><img src="{{ URL::asset('images/ic_check.svg')}}" alt="" class="mr-2 pr-1">Submit</button><br>
        <button type="reset" class="btn btn-primary btn btn-2 float-right"><svg width="16px" height="16px" class="sv svg-reset vam mr-2 pr1" aria-hidden="true" role="img" width="18" height="18"> <use href="#sv-reset" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#sv-reset"></use> </svg> Reset</button>
      </div>
      <div class="poster-btn text-center">
         <span class="b-r-6">Poster<font>drops</font></span>
      </div>
    </div>
</form>
</div>
@stop 
@section('css')
<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.fileupload.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.fileupload-ui.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/edit-art.css') }}">
<!-- <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/fileinput.min.css') }}"> -->
<style type="text/css">
   .red_text{
              color:red;
            }
   .selectize-control.multi .selectize-input [data-value] , .selectize-control.multi .selectize-input [data-value].active{background-color: #efefef !important; text-shadow: none;background-image: none;color: #333333}
   .selectize-dropdown{font-size: 16px}
   .selectize-dropdown .active , .selectize-dropdown .active.create {background-color: #efefef; color: #333333}
   .selectize-control.plugin-remove_button [data-value]{padding-right: 20px !important}
   .selectize-control.multi .selectize-input > div{padding-left: 6px; padding-right: 6px}
    /*.selectize-control.multi .selectize-input, .selectize-control.multi .selectize-input input {cursor: pointer;}
    .selectize-control.multi .selectize-input, .selectize-dropdown.multi {border-color: #b8b8b8;}
    .selectize-control.multi .selectize-input {background-color: #f9f9f9; background-image: linear-gradient(to bottom, #fefefe, #f2f2f2); background-repeat: repeat-x;box-shadow: 0 1px 0 rgba(0, 0, 0, 0.05), inset 0 1px 0 rgba(255, 255, 255, 0.8)}*/
   .file-upload-indicator, .file-actions{ display: none; }
   .sortable li{position: relative; float: left; padding-left: 0px; padding-right: 0px}
   .sortable .close {
       position: absolute;
       bottom: 0px;
       right: 0px;
       z-index: 100;
   }
   .sortable .move {
       position: absolute;
       top: 0px;
       left: 0px;
   }
   .sortable li button{background: none; background-color: black;border: none;cursor: move;}
   .sortable li button:hover{background-color: #ccc; background: #ccc !important;}
   .file-caption.form-control input{padding-left: 0.75rem !important}
   .form-group span i{font-size: 8px; vertical-align: top; margin-top: 4px}
  .fade.in {
    opacity: 1
  }
  .fileupload-buttonbar .btn, .template-upload .btn, .template-download .btn{padding: 6px !important; font-size: 10px}
  .table-striped td{padding:0.4rem;}
  .blueimp-gallery>.next, .blueimp-gallery>.prev{color: #fff !important}
</style>
@stop
@section('jsscript')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- <script type="text/javascript" src="{{ URL::asset('js/fileinput.min.js') }}"></script> -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}" style="min-width:30px"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<script type="text/javascript" src="{{ URL::asset('js/plugins/purify.min.js') }}"></script>
<script src="{{ URL::asset('js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ URL::asset('js/url.min.js') }}"></script>
<script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="https://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="{{ URL::asset('js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ URL::asset('js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ URL::asset('js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ URL::asset('js/jquery.fileupload-image.js') }}"></script>
<!-- The File Upload validation plugin -->
<script src="{{ URL::asset('js/jquery.fileupload-validate.js') }}"></script>
<script src="{{ URL::asset('js/jquery.fileupload-ui.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>
<script type="text/javascript">
$( document ).ready(function() {
    $.validator.addMethod(
     "imageRequired", 
     function(value, element) {
            // alert($('.image_ul li').length);
         if($('.image_ul li').length > 0 || element.value != ''){
            return true;
         }
         return false;
     },
     "This field is requried"
   );
   $(".fileinput").rules("add", { 
     imageRequired:true
   });
   var $selectize =  $('#artist_id, #band_id, #venue_id, #color_id, #manufacturer_id, #technique_id, #keyword_id').selectize({
        create: function(input){    //  Capitalize
            return {'value':'#new#'+input.toLowerCase().replace(/\b[a-z]/g, function(letter) {return letter.toUpperCase();}), 
            'text':input.toLowerCase().replace(/\b[a-z]/g, function(letter) {return letter.toUpperCase();})};
        },
         // closeAfterSelect: true,
         // onItemAdd: function() {
         //    // this.close();
         //    // alert('dfd');
         //    $(".selectize-dropdown").css("opacity", "0");
         //    setTimeout(function() {
         //       // alert('aaa');
         //       this.blur();
         //       this.close();
         //       $(".selectize-dropdown").css("opacity", "1");
         //    }, 1000);
         //    // this.blur();
         // },
        plugins: ['remove_button'],
        placeholder: 'Select...'
   });
   $selectize.each(function(index){
      var selectize = $selectize[index].selectize;
      selectize.on("item_add", function()  {
         // alert('dfd');
         $(".selectize-dropdown").css("opacity", "0");
         setTimeout(function() {
            selectize.close();
            // selectize.blur();
            $(".selectize-dropdown").css("opacity", "1");
         }, 100);
      });
      selectize.on("item_remove", function()  {
         // alert('dfd');
         $(".selectize-dropdown").css("opacity", "0");
         setTimeout(function() {
            selectize.close();
            // selectize.blur();
            $(".selectize-dropdown").css("opacity", "1");
         }, 100);
      });
   });
   var $selectize2 = $('#event_id, #movietitle_id, #actor_id, #paper_id, #size_w_id, #size_h_id, #series_id, #original_price_id').selectize({
        create: function(input){    //  Capitalize
            return {'value':'#new#'+input.toLowerCase().replace(/\b[a-z]/g, function(letter) {return letter.toUpperCase();}), 
            'text':input.toLowerCase().replace(/\b[a-z]/g, function(letter) {return letter.toUpperCase();})};
        },
        placeholder: 'Select...',
        maxOptions: 100000,
        // sortField:  function(item1, item2) {
        //   // alert($(this));
        //   // console.log($(this).attr('id'));
        //   return item1.value - item2.value;
        // },
    });
   // var sort_array = [0,4,6];  //  event, paper, series
   // sort_array.forEach(function(index){
   //  // alert(index);
   // var tmp = $selectize2[index].selectize;
   // console.log(tmp);
   //    tmp.settings.sortField = [{ field: 'text', direction: 'asc' }];
   //  });
   $('#size_w_id-selectized, #size_h_id-selectized, #original_price_id-selectized').on('keypress',function(evt){
      el = this;
       var charCode = (evt.which) ? evt.which : event.keyCode;
       var number = el.value.split('.');
       if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
           return false;
       }
       //just one dot (thanks ddlab)
       if(number.length>1 && charCode == 46){
            return false;
       }
       //get the carat position
       // var caratPos = getSelectionStart(el);
       // var dotPos = el.value.indexOf(".");
       // if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
       //     // return false;
       // }
       return true;

    });
   $('#quantity_id-selectized').on('keypress',function(evt){
      el = this;
       var charCode = (evt.which) ? evt.which : event.keyCode;
       var number = el.value.split('.');
       if ( charCode > 31 && (charCode < 48 || charCode > 57)) {
           return false;
       }
       // if(charCode == 46){
       //      return false;
       // }
       return true;

    });
    $('#printcategory, #printtype, #printversion, #moviegenre_id, #quantity_id, #status_id, #num_series_id, #signed, #numbered, #printings').selectize({
        placeholder: 'Select...',
        // sortField:  [{ field: 'text', direction: 'asc' }],
    });
    $('#printcategory').on('change', function(){

        var tmp = $('#printtype').val();
        $('#printtype')[0].selectize.clear();
        if($(this).val() == 'Art Poster'){
            $('.artposter_div').show();
            // $('.non_artprint_div').hide();
            $('#printtype')[0].selectize.removeOption('Show Edition');
        }else{
            $('.artposter_div').hide();
            // $('.non_artprint_div').show();
            $('#printtype')[0].selectize.addOption({value:'Show Edition',text:'Show Edition'});
        }
        // alert(tmp);
        if($(this).val() == 'Concert Poster'){
            $('.concertposter_div').show();
            // $('#printtype').append('<option value="option1">option1</option>');
            // $('#printtype').selectize({placeholder: 'Select...'});
            // $('#printtype')[0].selectize.addItem('Show Edition');
        }else{
            $('.concertposter_div').hide();
            // $('#printtype option[value="option1"]').remove();
            // $('#printtype').selectize({placeholder: 'Select...'});
        }
        if($(this).val() == 'Event Poster'){
          $('#printtype')[0].selectize.addOption([{value:'Fan Art',text:'Fan Art'},{value:"Licensed",text:"Licensed"}]);
          // $('#printtype')[0].selectize.sortField
            $('.eventposter_div').show();
        }else{
          $('#printtype')[0].selectize.removeOption('Fan Art');
          $('#printtype')[0].selectize.removeOption('Licensed');
          $('.eventposter_div').hide();
        }
        if($(this).val() == 'Movie Poster'){
            $('.movieposter_div').show();
        }else{
          $('.movieposter_div').hide();
        }

        // $('#printtype')[0].selectize.refreshOptions(false);
        // $('#printtype').selectize()[0].selectize.destroy();
        // $('#printtype').selectize({placeholder: 'Select...'});
        if($(this).val() != ''){
        // alert('dfd');
            $('#printtype').removeAttr('disabled');
            $('#printtype')[0].selectize.enable();
        }else{
            $('#printtype').attr('disabled',true);
            $('#printtype')[0].selectize.disable();
        }
        $('#printtype')[0].selectize.setValue(tmp);

    });
    $('#printcategory').trigger('change');
    $('#printversion').on('change', function(){
          // alert($(this).val());
        if($(this).val() == 'Variant'){
            $('.variant_div').show();
        }else{
            $('.variant_div').hide();
        }
    });
    $('#printversion').trigger('change');
    $('.datepicker').datepicker({
        dateFormat: "mm/dd/yy",
        changeMonth: true,
        changeYear: true
    });
    $('.yearpicker').yearpicker({
        startYear: 2015,
        endYear: 2018
    });
   $( ".sortable" ).sortable({
      handle:".move",
      cancel: '',
      stop: function( event, ui ) {
         // alert($(".sortable").sortable("toArray"));
         $('#imageinorder').val($(".sortable").sortable("toArray"));
      }
   });
    var xhr;
    var select_state, $select_state;
    var select_country, $select_country, select_city, $select_city;
    $select_country = $('#country_id').selectize({
      sortField:  [{ field: 'text', direction: 'asc' }],
      onChange: function(value){
        // alert(value);return false;
        // if (!value.length) return;
        // if(value == '1'){
        //   select_state.settings.create = false;
        // }else{
        //   select_state.settings.plugins = ['remove_button'];
        //   select_state.settings.create = function(input){    //  Capitalize
        //       return {'value':'#new#'+input.toLowerCase().replace(/\b[a-z]/g, function(letter) {return letter.toUpperCase();}), 
        //       'text':input.toLowerCase().replace(/\b[a-z]/g, function(letter) {return letter.toUpperCase();})};
        //   };
        // }
        // $this = $(this);
        select_state.enable();
        select_state.clearOptions();
        select_state.load(function(callback) {
          xhr && xhr.abort();
          xhr = $.ajax({
            url: "{{ url('admin/get_states')}}",
            method: "post",
            dataType: 'json',
            data: 'country_id='+value,
            success: function(results) {
              select_state.enable();
              callback(results.states);
              // alert(results);
            },
            error: function() {
              callback();
            }
          })
        });
      }
    });
    $select_state = $('#state_id').selectize({
      placeholder: 'Select...',
      sortField:  [{ field: 'text', direction: 'asc' }],
      onChange: function(value){
        // alert(value);
        // alert(select_city);
        if (value.length){
          select_city.enable();
          select_city.clearOptions();
          select_city.load(function(callback) {
            xhr && xhr.abort();
            xhr = $.ajax({
              url: "{{ url('admin/get_cities')}}",
              method: "post",
              dataType: 'json',
              data: 'state_id='+value,
              success: function(results) {
                select_city.enable();
                callback(results.cities);
                // alert(results);
              },
              error: function() {
                callback();
              }
            })
          });
        }else{
          select_city.disable();
        }
      }
    });
    $select_city = $('#city_id').selectize({
      // plugins: ['remove_button'],
      placeholder: 'Select...',
      // sortField:  [{ field: 'text', direction: 'asc' }],
      // create: function(input){    //  Capitalize
      //     return {'value':'#new#'+input.toLowerCase().replace(/\b[a-z]/g, function(letter) {return letter.toUpperCase();}), 
      //     'text':input.toLowerCase().replace(/\b[a-z]/g, function(letter) {return letter.toUpperCase();})};
      // },
    });
    select_country = $select_country[0].selectize;
    select_state  = $select_state[0].selectize;
    select_city = $select_city[0].selectize;
    // select_state.on("item_add", function()  {
    //    // alert('dfd');
    //    $(".selectize-dropdown").css("opacity", "0");
    //    setTimeout(function() {
    //       select_state.close();
    //       // selectize.blur();
    //       $(".selectize-dropdown").css("opacity", "1");
    //    }, 100);
    // });
    // select_state.on("item_remove", function()  {
    //    // alert('dfd');
    //    $(".selectize-dropdown").css("opacity", "0");
    //    setTimeout(function() {
    //       select_state.close();
    //       // selectize.blur();
    //       $(".selectize-dropdown").css("opacity", "1");
    //    }, 100);
    // });
    // select_city.on("item_add", function()  {
    //    // alert('dfd');
    //    $(".selectize-dropdown").css("opacity", "0");
    //    setTimeout(function() {
    //       select_city.close();
    //       // selectize.blur();
    //       $(".selectize-dropdown").css("opacity", "1");
    //    }, 100);
    // });
    // select_city.on("item_remove", function()  {
    //    // alert('dfd');
    //    $(".selectize-dropdown").css("opacity", "0");
    //    setTimeout(function() {
    //       select_city.close();
    //       // selectize.blur();
    //       $(".selectize-dropdown").css("opacity", "1");
    //    }, 100);
    // });
    select_state.disable();
    select_city.disable();
    $('.selectize-control.multi').addClass('single');
    select_country.trigger('change');

   $('#getorder').on('click',function(){
      // alert($(".sortable").sortable("toArray"));
   });
   $('.sortable button.close').on('click',function(){
      if(confirm('Are you sure you want to delete this image?')){
         var $this = $(this);
         $.ajax({
            url: "{{ url('admin/deleteImage')}}",
            method: 'post',
            data: 'id=<?=$detail->id?>&image='+$this.closest('li').data('image')
         }).done(function(response){
            // alert(response);
            $this.closest('li').remove();
            // alert($('#imageinorder').val().replace($this.closest('li').data('image'),''));
            $('#imageinorder').val($('#imageinorder').val().replace($this.closest('li').data('image'),'')) ;
         })
      }
   });
});
function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}

</script>
@stop 
