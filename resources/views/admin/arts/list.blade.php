<!--DATA TABLE HEADER-->
<style>.select2-container{width:250px !important;margin-top: 0px !important}</style>
<div class="datatable-header">
    <!-- <div id="DataTables_Table_2_filter" class="dataTables_filter">
        <label><input class="" id="search" value="{{ Session::get('user_search') }}"
                   onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('art_list')}}?ok=1&search='+this.value)"
                   placeholder="Search..."
                   type="text">
        </label>
    </div> -->
    <!-- <div id="DataTables_Table_0_filter" class="dataTables_filter">
        <button id='btn_search' type="button" class="btn  btn-sm btn-default" onclick="ajaxLoad('{{url('art_list')}}?ok=1&search='+$('#search').val()+'&user_type='+$('#user_type').val())"><i class="glyphicon glyphicon-search"></i> Go</button>
        <button type="button" id='btn_show_all' class="btn  btn-sm btn-default" onclick="ajaxLoad('{{url('art_list')}}?ok=1&search=&field=&sort=')"><i class="icon-reset"></i>  Reset</button>
    </div> -->
    <div class="dataTables_length" id="DataTables_Table_0_length">
        <label><span>Show:</span>
            <select name="show" aria-controls="DataTables_Table_2" class="show_dropdown" tabindex="-1" onchange="ajaxLoad('{{url('art_list')}}?show='+$(this).val())" >
                <option {{ (Session::get('user_show')==5)?"selected":"" }}>5</option>
                <option {{ (Session::get('user_show')==10)?"selected":"" }}>10</option>
                <option {{ (Session::get('user_show')==15)?"selected":"" }}>15</option>
                <option {{ (Session::get('user_show')==20)?"selected":"" }}>20</option>
                <option {{ (Session::get('user_show')==50)?"selected":"" }}>50</option>
                <option {{ (Session::get('user_show')==100)?"selected":"" }}>100</option>
                <option {{ (Session::get('user_show')==500)?"selected":"" }}>500</option>
            </select>
            
        </label>
    </div>
</div>
<!-- DATA TABLE CENTEr -->
    <table class="table table-togglable table-hover dataTable" id="DataTables_Table_2" role="grid" aria-describedby="DataTables_Table_2_info">
    <thead>
        <tr class="bg-blue-oakray">
            <th>#</th>
            <th rowspan="1" colspan="1" class="{{ Session::get('art_field')=='name'?(Session::get('art_sort')=='asc'?'sorting_asc':'sorting_desc'):'sorting' }}" onClick="javascript:ajaxLoad('art_list?field=name&sort={{Session::get("art_sort")=="asc"?"desc":"asc"}}')"  data-hide="x-small,small">
                    Name
            </th>
            <th rowspan="1" colspan="1" class="{{ Session::get('art_field')=='year'?(Session::get('art_sort')=='asc'?'sorting_asc':'sorting_desc'):'sorting' }}" onClick="javascript:ajaxLoad('art_list?field=year&sort={{Session::get("art_sort")=="asc"?"desc":"asc"}}')" data-hide="x-small,small">
                    Year
            </th>
            <th rowspan="1" colspan="1" class="{{ Session::get('art_field')=='printtype'?(Session::get('art_sort')=='asc'?'sorting_asc':'sorting_desc'):'sorting' }}" onClick="javascript:ajaxLoad('art_list?field=printtype&sort={{Session::get("art_sort")=="asc"?"desc":"asc"}}')" data-toggle="true"> 
                    Print Type 
            </th>
            <th rowspan="1" colspan="1" >Artists</th>
            <th rowspan="1" colspan="1" style='width:100px'>
                &nbsp;&nbsp;Action&nbsp;&nbsp;
            </th>
        </tr>
        </thead>
        <tbody>
        <?php $i = $arts->firstItem(); ?>
        @if(count($arts))    
        @foreach($arts as $key=>$art)
        <?php   if ($art->status=='I'){ echo "<tr class='disabled_status'>"; } 
                else if($i % 2 === 0){ echo "<tr class='tableRows'>"; } 
                else { echo "<tr>"; }                
                ?>            
                <td>{{ $i++ }}</td>
                <td class='wrap'>{{ $art->name }}</td>
                <td class='wrap'>{{ $art->year }}</td>
                <td class='wrap'>{{ $art->printtype }}</td>
                <td class='wrap'>{{ $art->artist }}</td>
                <td class='text-center'>
                    <ul class="icons-list">
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li data-popup="tooltip" title="Edit"><a id='btn_edit{{ $i}}' href="{{URL('')}}/admin/art/{{ $art->id }}/edit"><i class="icon-pencil7 text-primary"></i></a></li>
                        <li data-popup="tooltip" title="Delete"><a id='btn_delete{{ $i}}' href="javascript:void(0)" onclick="delete_conf('{{ $art->id }}','Art','','art');"><i class="icon-trash delete text-danger"></i></a></li> 
                    </ul>
                </td>
            </tr>
        @endforeach
        @else
            <tr>
                <td colspan='10' class='text-center'>No data found.</td>
            </tr>
        @endif
        </tbody>
    </table>        

<!--  DATA TABLE FOOTER- -->
<script>
    $('.pagination a').on('click', function (event) {
        event.preventDefault();
        ajaxLoad($(this).attr('href'));
    });    
</script>       