@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @include('flash-message')
                    You are logged in!
                </div>
            </div>
        </div>
        <div class="col-md-8">
            @if($service == 'facebook' || $service == 'google')
            <div class="title m-b-md">
                Welcome {{ $details->user['name']}} ! <br> Your email is : {{
                $details->user['email'] }} <br> You are {{ $details->user['gender'] }}.
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
