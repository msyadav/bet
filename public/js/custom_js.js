$(document).ready(function(e) {
	$.ajaxSetup({	//----------REDIRECT TO LOGIN PAGE IF SESSIONOUT DURING AJAX CALL-----------------------
            beforeSend:function(result) {
              
                    $(".loading").show();
                    },
            complete: function(result) {
                            if(result.responseText == '{"error":"Unauthenticated."}'){
                                    window.location.href = "login";			
                            }
                        $(".loading").hide();    
                    },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }

        });
    // START WIZARD STEPS VALIDATION 
    //	START Setup validation
    
    var validator = $(".form-validate-jquery , .steps-validation").validate({
        ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
        errorClass: 'validation-error-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).parent().removeClass('validate_tr');
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {
            
            // Styled checkboxes, radios, bootstrap switch
            if (element.hasClass('multiselect')) {
                    error.insertAfter(element.next('.btn-group'));
                }
            else if ( element.hasClass('colorpicker')) {
                    error.insertAfter(element.next('.sp-replacer'));
                    
                }    
            else if ( element.hasClass('file-input_control')) {
                error.appendTo( element.parent().parent().parent().parent());
              
            }    
            else if ( $(element).hasClass('selectized') ){
                error.insertAfter($(element).nextAll('.selectize-control'));
            }
            else if ( $(element).hasClass('fileinput') ){
                error.appendTo(element.parent().parent().parent().parent());
            }
            else if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline') ) {
                        error.appendTo( element.parent().parent().parent().parent() );
                }
                else {
                       
                    if(element.hasClass("lettergroup") || element.hasClass("use_type_radio"))
                    {
                        error.insertAfter( element.parent().parent() );
                        
                    }
                    else if(element.hasClass("fuel_typegroup"))
                    {
                        error.appendTo( element.parent().parent().parent());
                    }
                    else
                    {
                        error.insertBefore( element.parent().parent().parent().parent().parent());
                    }
                }
            }
            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parshow_edit_typesent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                if(element.parents('table').hasClass('validate_table') )
                {
                    element.parent().addClass('validate_tr');
                }
                else
                {
                    error.appendTo( element.parent() );
                }
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
                
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }
            else if(element.parents('table').hasClass('validate_table') )
            {
                element.parent().addClass('validate_tr');
              //  alert('add validate_tr');
            }
            else {
                
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        rules: {
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 2
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            },
			planner_weekly_required:{
				planner_weekly_required:true
			},
			planner_weekly_duplicate:{
				planner_weekly_duplicate:true
			},
            printcategory: "required",
            printtype: "required",
            printversion: "required",
            "artist_id[]": "required"
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        }
    });

    // RESET POPUP FORM (TO clear old validation messages..)
    setTimeout(function(){ 
        $('*[data-dismiss="modal"]').click(function() {
            if($(this).closest("form").length > 0)
            {
                var form_id = $(this).closest("form").attr('id');
                if(form_id!='undefined')
                {$("#"+form_id).validate().resetForm();}
            }
        });
    }, 1000);
    
    
    // Reset form
  //   $('button[type="reset"]').on('click', function() {
  //       validator.resetForm();
		// this.form.reset();  
		// $('form').find('input[type=checkbox].control-primary').each(function(){
		// 	if($(this).is(":checked")){
		// 		$(this).closest('span').addClass('checked');
		// 	}else{
		// 		$(this).closest('span').removeClass('checked');
		// 	}
		// });
		// $('form .select-size-xs').each(function(){
		// 	$(this).trigger('change');
		// });
		// // for (instance in CKEDITOR.instances){
		// //    CKEDITOR.instances[instance].setData(CKEDITOR.instances[instance].value);
		// // }		
  //   });
    //	END Setup validation

});
//---------------BACK BUTTON AND CANCEL BUTTON CONFIRMATION-------------------------
function back_cancel_confirm(type,url)
{
    if(type=='back')
    {
        $('#back_cancel_confirm').modal('show');
        $("#back_cancel_title").html("Back");
        $("#back_cancel_type").html("go "+type);    
        $("#confirm_action_btn").attr("onclick","confirm_back_action('"+url+"')");
    }
    else
    {
        $('#back_cancel_confirm').modal('show');
        $("#back_cancel_title").html("Cancel");
        $("#back_cancel_type").html(type);
        $("#confirm_action_btn").attr("onclick","confirm_cancel_action(this)");
    }
}
function confirm_back_action(url)
{
    window.location=url;
}
function confirm_cancel_action(ele)
{
    ele.modal("hide");
}
//-----------SHOW NOTIFICATION MESSAGES-----------------
function show_notification(type, message) {
var n = noty({
            layout: 'topCenter',
            theme: 'defaultTheme', // or relax
            type: type, // success, error, warning, information, notification
            text: message,
            timeout: 3000,
            animation: {
                    open: {height: 'toggle'},
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 500 // opening & closing animation speed
            }
    });
}
//---------LOAD PAGINATION LIST DATA-------------------
function ajaxLoad(filename, content) {

    $('#search').attr('disabled','disabled');

    content = typeof content !== 'undefined' ? content : '';
    $(".loading").show();
    $.ajax({
        type: "GET", 
        url: filename,  // no base url required filename is includes baseurl.
        data: content,
        contentType: false,
        success: function (data) {
            $("#content").html(data);
            $(".loading").hide();
            if (filename.indexOf("jobs_list") >= 0)
            {
                $('.page-header').load(location.href + ' .page-header');
            }
        },
        error: function (xhr, status, error) {
            alert("Whoops, looks like something went wrong");
        }
    });

    setTimeout(function(){ 
                    $('#search').focus(); 
                    var tmpStr = $('#search').val();
                    $('#search').val('');
                    $('#search').val(tmpStr);
            },500);

    $('#search').removeAttr('disabled','disabled');
}
//----------CHANGE STATUS-----------------------
function changeStatus(id, model, status ,url) {
    $.ajax({
        type: "POST",
           url: BASE_URL+"/change_status",
           dataType: 'json',
           data: {
               id: id,
               status: status,
               model: model               
           },
           success: function (data) {               
               if(data['status']=='success')
               {
                   ajaxLoad(url);
               }
               else
               {
                    $("#inform_popup_title").html('Warning');
                    $("#inform_popup_msg").html(data['message']);
                    $("#inform_popup").modal('show');
               }
           }
    });
}
//--------DELTE CONFIRMATION POPUP--------------
function delete_conf(id,model,item,url, parent_module='', parent_module_id=''){
    $('#module_title').html(" "+item);
    $('#delete-btn').attr("onclick","delete_item('"+id+"','"+model+"','"+item+"','"+url+"','"+parent_module+"','"+parent_module_id+"')");
    $('#delete_confirm').modal('show');
    return false;
}
//-----------DELETE THE ITEM------------
function delete_item(id,model,item,url, parent_module='', parent_module_id='')
{
    $.ajax({
           type: "POST",
           url: BASE_URL+"/delete_item",
           dataType: 'json',
           data: {
               id: id,
               model: model,
               parent_module: parent_module,
               parent_module_id: parent_module_id
           },
           success: function (data) {
               if(data['status']=='success')
               {
                    console.log(url+'_list');
                    ajaxLoad(url+'_list');
                    $('#delete_confirm').modal('hide');
                    show_notification(data['status'],data['message']);
               }
               else
               {
                    $('#delete_confirm').modal('hide');
                    $("#inform_popup_title").html('Warning');
                    $("#inform_popup_msg").html(data['message']);
                    $("#inform_popup").modal('show');
               }
           }
    });  
}
//-----------DELETE MULTI ITEM------------
function delete_multi_item(model,url,table)
{
    var id = $("#multi_del_id_arr").val();
    $.ajax({
           type: "POST",
           url: BASE_URL+"/multi_delete_item",
           dataType: 'json',
           data: {
               id: id,
               model: model,
               table: table
           },
           success: function (data) {
               if(data['status']=='success')
               {
                   ajaxLoad(url+'_list');
                   $('#multi_delete_confirm').modal('hide');
                   show_notification(data['status'],data['message']);
               }
               else
               {
                    $('#delete_confirm').modal('hide');
                    $("#inform_popup_title").html('Warning');
                    $("#inform_popup_msg").html(data['message']);
                    $("#inform_popup").modal('show');
               }
           }
    });  
}
//--------DELTE CONFIRMATION POPUP ON EDIT PAGE--------------
function delete_conf_edit(id,model,item,url,tableid, parent_module = '', parent_module_id = ''){
    $('#module_title').html(model+" "+item);
    $('#delete-btn_edit').attr("onclick","delete_item_edit('"+id+"','"+model+"','"+item+"','"+url+"','"+tableid+"','"+parent_module+"','"+parent_module_id+"')");
    $('#delete_confirm_edit').modal('show');
    return false;
}
//-----------DELETE THE ITEM------------
function delete_item_edit(id,model,item,url, tableid, parent_module = '', parent_module_id = '')
{
    $.ajax({
           type: "POST",
           url: BASE_URL+"/delete_item",
           dataType: 'json',
           data: {
               id: id,
               model: model,
               parent_module: parent_module,
               parent_module_id: parent_module_id
           },
           success: function (data) {
               if(data['status']=='success')
               {
                   $('#'+tableid+' tbody tr[id='+id+']').remove();
                   $('#delete_confirm_edit').modal('hide');
                   $("#job_doc_table").load(window.location + " #job_doc_table");
                   $("#job_PO_table").load(window.location + " #job_PO_table");
                   show_notification(data['status'],data['message']);
               }
           }
    });  
}
