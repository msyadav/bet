/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: BASE_URL+'/art/upload_image'
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 999000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }
    $('#fileupload').on('fileuploaddone',function(e, data){
        $.each(data.result.files, function (index, file) {
           var tmp = $('#image').val() + ','+file.name;
           $('#image').val(tmp); 
        });
        // alert($('#image').val());
        // $('#image').val();
        // alert(data.toString());
    }).on('fileuploaddestroy',function(e,data){
        console.log(data.url);
        console.log(url('?file',data.url));
        var filename = url('?file',data.url);
        // console.log(JSON.stringify(data));
        // console.log(JSON.stringify(e));
        // $.each(data, function (index, file) {
            // console.log(file);
           // var tmp = $('#image').val() + ','+file.name;
           // $('#image').val(tmp); 
           // var tmp = $('#image').val().split(',');
           // $('#image').val(tmp.splice($.inArray(filename, tmp),1).join(','));
           $('#image').val($('#image').val().replace(','+filename,'').replace(filename,''));
           // alert($('#image').val().split(',').splice($.inArray(file.name, arr),1).join(','));
        // });
    });

});
